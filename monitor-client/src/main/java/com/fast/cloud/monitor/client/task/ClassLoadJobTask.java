package com.fast.cloud.monitor.client.task;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.fast.cloud.framework.core.model.FastResult;
import com.fast.cloud.monitor.client.entity.MonitorClassLoad;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.entity.MonitorServerConn;
import com.fast.cloud.monitor.client.job.MonitorAbstractJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzTriggerInitialize;
import com.fast.cloud.monitor.client.service.MonitorClassLoadService;
import com.fast.cloud.monitor.client.service.MonitorServerConnService;
import com.fast.cloud.monitor.client.task.runnable.CallableValue;
import com.fast.cloud.monitor.client.task.runnable.MonitorJobRunnable;
import com.fast.cloud.monitor.constant.MonitorUrl;
import com.google.common.collect.Maps;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * jvm数据收集定时任务
 */
@QuartzJob(jobKeyName = "classLoadJob类加载", jobKeyGroup = "thread")
@QuartzTriggerInitialize(triggerKeyName = "classLoadJob类加载",triggerKeyGroup = "thread", cronExpress = "0 */1 * * * ?")
public class ClassLoadJobTask extends MonitorAbstractJob {
    @Autowired
    private MonitorClassLoadService monitorClassLoadService;
    @Autowired
    private MonitorServerConnService monitorServerConnService;
    @Autowired
    private ThreadPoolExecutor threadPoolExecutor;

    /**
     * 类加载信息收集
     */
    public void pullClassLoadInfo() {
        List<MonitorServerConn> list = monitorServerConnService.listServerConn();
        List<Future<CallableValue<FastResult<MonitorClassLoad>>>> taskList = new ArrayList<>(list.size());
        for (MonitorServerConn conn : list) {
            String url = conn.getUrl() + MonitorUrl.TINFO_CLASS_LOAD;
            Map<String, Object> param = Maps.newHashMap();
            param.put("pid", conn.getPid());
            MonitorJobRunnable<FastResult<MonitorClassLoad>> monitorJobRunnable = new MonitorJobRunnable(url, param, conn,FastResult.class);
            Future<CallableValue<FastResult<MonitorClassLoad>>> future = threadPoolExecutor.submit(monitorJobRunnable);
            taskList.add(future);
        }

        // 处理结果
        List<MonitorClassLoad> httpResult = new ArrayList<>(taskList.size());
        for (Future<CallableValue<FastResult<MonitorClassLoad>>> f : taskList) {
            try {
                CallableValue<FastResult<MonitorClassLoad>> mapCallableValue = f.get(5000L, TimeUnit.MILLISECONDS);
                MonitorClassLoad classLoad = JSON.parseObject(JSON.toJSONString(mapCallableValue.getData().getResult()), MonitorClassLoad.class);
                MonitorServerConn conn = mapCallableValue.getConn();
                classLoad.setIp(conn.getIp());
                classLoad.setApplicationName(conn.getApplicationName() + ":" + conn.getPort());
                classLoad.setId(null);
                httpResult.add(classLoad);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (CollectionUtil.isEmpty(httpResult)) {
            return;
        }
        //插入记录
        monitorClassLoadService.saveBatch(httpResult);
    }

    @Override
    protected void doExecuteInternal(JobExecutionContext context, MonitorJob scheduleJob) throws JobExecutionException {
        pullClassLoadInfo();
    }
}
