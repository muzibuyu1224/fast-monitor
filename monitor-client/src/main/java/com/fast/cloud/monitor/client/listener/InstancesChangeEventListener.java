package com.fast.cloud.monitor.client.listener;

import com.alibaba.fastjson.JSON;
import com.alibaba.nacos.client.naming.event.InstancesChangeEvent;
import com.alibaba.nacos.common.notify.NotifyCenter;
import com.alibaba.nacos.common.notify.listener.Subscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @Description: 服务实例变更事件处理
 * @Author: muzibuyu1224
 * @Date: 2021/9/23
 * @Version: V1.0
 */
@Component
public class InstancesChangeEventListener
        extends Subscriber<InstancesChangeEvent> implements ApplicationEventPublisherAware {
    private static final Logger logger = LoggerFactory.getLogger(InstancesChangeEventListener.class);
    private ApplicationEventPublisher applicationEventPublisher;

    @PostConstruct
    private void post() {
        NotifyCenter.registerSubscriber(this);
    }


    /**
     * 借助redis实现吧  好像本身不支持  借助redis监听订阅模式
     * Event callback.
     *
     * @param event {@link Event}
     */
    @Override
    public void onEvent(InstancesChangeEvent event) {
        applicationEventPublisher.publishEvent(new ServerChangeEvent(new Object()));
    }

    /**
     * Type of this subscriber's subscription.
     *
     * @return Class which extends {@link Event}
     */
    @Override
    public Class<? extends com.alibaba.nacos.common.notify.Event> subscribeType() {
        return InstancesChangeEvent.class;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}
