package com.fast.cloud.monitor.client.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 系统监控配置表
 */
@Data
public class MonitorConfigVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;


	/**
	 * 配置名称
	 */
	private String name;


	/**
	 * 配置code
	 */
	private String code;


	/**
	 * 配置值
	 */
	private String value;


	private Long createBy;


	private String createByAccount;

	/**
	 * <h2>创建时间</h2>
	 */
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTime;

	/**
	 * 更新者ID
	 */
	private Long updateBy;

	/**
	 * 更新者ID
	 */
	private String updateByAccount;

	/**
	 * 更新时间
	 */
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updateTime;
}
