package com.fast.cloud.monitor.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;


public interface MonitorJobMapper extends BaseMapper<MonitorJob> {

    @Update("update monitor_job set status = #{status} where id=#{id}")
    int updateStatus(@Param("status") int status, @Param("id") Long id);
}
