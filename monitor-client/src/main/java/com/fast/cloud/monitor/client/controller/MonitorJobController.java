package com.fast.cloud.monitor.client.controller;

import com.fast.cloud.framework.core.model.FastResult;
import com.fast.cloud.framework.data.pagedata.PageData;
import com.fast.cloud.monitor.client.service.MonitorJobService;
import com.fast.cloud.monitor.client.vo.MonitorJobVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


@RestController
@RequestMapping("/job")
public class MonitorJobController {

    @Resource
    private MonitorJobService monitorJobService;

    @PostMapping("/page")
    public FastResult<PageData<MonitorJobVo>> list(@RequestBody MonitorJobVo monitorJobVo) {
        // 查询列表数据
        PageData<MonitorJobVo> page = monitorJobService.page(monitorJobVo);
        return FastResult.build(page);
    }

    @GetMapping("/info/{id}")
    public FastResult<MonitorJobVo> info(@PathVariable("id") Long id) {
        MonitorJobVo job = monitorJobService.getMonitorJobById(id);
        return FastResult.build(job);
    }

    @PostMapping("/save")
    public FastResult<Long> save(@RequestBody MonitorJobVo scheduleJobVo) {
       Long id =  monitorJobService.insertJob(scheduleJobVo);
        return FastResult.build(id);
    }

    @PutMapping("/update")
    public FastResult<Boolean> update(@RequestBody MonitorJobVo monitorJobVo) {
        monitorJobService.updateJob(monitorJobVo);
        return FastResult.build(true);
    }

    @DeleteMapping("/delete/{id}")
    public FastResult<Boolean> delete(@PathVariable("id") Long id) {
        monitorJobService.removeJob(id);
        return FastResult.build(true);
    }

    @PutMapping("/run/{id}")
    public FastResult<Boolean> run(@PathVariable("id") Long id) {
        monitorJobService.run(id);
        return FastResult.build(true);
    }

    @PutMapping("/pause/{id}")
    public FastResult<Boolean> pause(@PathVariable("id") Long id) {
        monitorJobService.pause(id);
        return FastResult.build(true);
    }

    @PutMapping("/resume/{id}")
    public FastResult<Boolean> resume(@PathVariable("id") Long id) {
        monitorJobService.resume(id);
        return FastResult.build(true);
    }
}
