package com.fast.cloud.monitor.client.job.core;

import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.quartz.QuartzProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * 配置Quartz
 * @author muzibuyu1224
 */
@Configuration
@ConditionalOnClass(SchedulerFactoryBean.class)
@EnableConfigurationProperties(QuartzProperties.class)
public class QuartzExtendConfig {

    @Bean
    @ConditionalOnMissingBean(SpringBeanJobFactory.class)
    public SpringBeanJobFactory springBeanJobFactory() {
        return new SpringBeanJobFactory();
    }

    /**
     * Quartz配置
     * @param quartzProperties
     * @param dataSource
     * @param jobFactory
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(SchedulerFactoryBean.class)
    public SchedulerFactoryBean schedulerFactoryBean(QuartzProperties quartzProperties,
                                                     DataSource dataSource,
                                                     @Qualifier("springBeanQuartzJobFactory") SpringBeanJobFactory jobFactory) {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        // add properties
        Properties properties = new Properties();
        properties.putAll(quartzProperties.getProperties());
        // 自动启动
        schedulerFactoryBean.setAutoStartup(quartzProperties.isAutoStartup());
        // 延时启动
        schedulerFactoryBean.setStartupDelay((int) quartzProperties.getStartupDelay().getSeconds());
        // 可选，QuartzScheduler 启动时更新己存在的Job，这样就不用每次修改targetObject后删除qrtz_job_details表对应记录了
        schedulerFactoryBean.setOverwriteExistingJobs(quartzProperties.isOverwriteExistingJobs());
        // set properties
        schedulerFactoryBean.setQuartzProperties(properties);
        // 设置job工厂
        schedulerFactoryBean.setJobFactory(jobFactory);
        // 设置数据库源
        schedulerFactoryBean.setDataSource(dataSource);
        //schedulerFactoryBean.setTaskExecutor();
        return schedulerFactoryBean;
    }

    /**
     * scheduler
     * @param factoryBean
     * @return
     */
    @Bean
    public Scheduler scheduler(@Qualifier("schedulerFactoryBean") SchedulerFactoryBean factoryBean) {
        return factoryBean.getScheduler();
    }
}
