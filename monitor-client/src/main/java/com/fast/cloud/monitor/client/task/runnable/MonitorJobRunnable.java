package com.fast.cloud.monitor.client.task.runnable;

import com.fast.cloud.monitor.client.entity.MonitorServerConn;
import com.fast.cloud.monitor.client.rest.RestClient;
import org.springframework.aop.framework.AdvisedSupport;
import org.springframework.aop.support.AopUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * monitor job
 *
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/10/9
 * @Version: V1.0
 */
public class MonitorJobRunnable<T> implements Callable<CallableValue<T>> {
    private final String url;
    private final MonitorServerConn conn;
    private final Map<String, Object> param;
    private final Class<T> clazz;

    public MonitorJobRunnable(String url, Map<String, Object> param, MonitorServerConn conn,Class<T> clazz) {
        this.url = url;
        this.param = param;
        this.conn = conn;
        this.clazz = clazz;
    }

    @Override
    public CallableValue<T> call() throws Exception {
        T t = (T) RestClient.get(url, param, clazz);
        CallableValue<T> value = new CallableValue(t, conn);
        return value;
    }
}
