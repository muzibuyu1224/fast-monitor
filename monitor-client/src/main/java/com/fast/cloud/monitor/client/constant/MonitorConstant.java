package com.fast.cloud.monitor.client.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 配置固定值
 *
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/10/7
 * @Version: V1.0
 */
public interface MonitorConstant {

    /**
     * 内存百分比
     */
    String MEM_LIMIT_PER = "serverMemLimitPer";

    /**
     * CPU百分比
     */
    String CPU_LIMIT_PER = "serverCpuLimitPer";


    /**
     * 数量(行数)
     */
    String THREAD_LIMIT_COUNT = "serverThreadLimitCount";


    String JOB_PARAM_KEY = "jsonParam";


    String SERVER_UP = "up";

    String SERVER_DOWN = "down";


    @Getter
    @AllArgsConstructor
    enum GcType {
        GC("gc"),
        GC_CAPACITY("gcCapacity"),
        GC_META_CAPACITY("gcMetaCapacity"),
        GC_NEW("gcNew"),
        GC_OLD("gcOld"),
        GC_NEW_CAPACITY("gcNewCapacity"),
        GC_OLD_CAPACITY("gcOldCapacity"),
        GC_UTIL("gcutil");
        private String type;
    }
}
