package com.fast.cloud.monitor.client.job.core;

import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.AnnotationMetadata;

/**
 * class扫描器
 * 扫描指定包下的class，将{@link QuartzJob}注解的类注册到spring容器中
 */
public class ClassPathQuartzJobScanner extends ClassPathBeanDefinitionScanner {
    public static final String JOB_PATH = "com.fast.cloud.monitor.client.job.annotation.QuartzJob";
    public ClassPathQuartzJobScanner(BeanDefinitionRegistry registry) {
        super(registry);
    }

    @Override
    protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
        AnnotationMetadata metadata = beanDefinition.getMetadata();
        return metadata.isConcrete()
                && metadata.hasAnnotation(JOB_PATH);
    }
}
