package com.fast.cloud.monitor.client.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fast.cloud.framework.core.base.PageCondition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 定时任务日志
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonitorJobLogVo implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;

    /**
     * 任务id
     */
    private Long jobId;

    /**
     * 任务id
     */
    private String jobName;

    /**
     * 任务id
     */
    private String jobGroup;

    /**
     * spring bean名称
     */
    private String scheduleName;

    /**
     * 参数
     */
    private String params;

    /**
     * 任务状态    0：成功    1：失败
     */
    private Integer status;

    /**
     * 失败信息
     */
    private String error;

    /**
     * 耗时(单位：毫秒)
     */
    private Integer times;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    private PageCondition pageCondition;
}
