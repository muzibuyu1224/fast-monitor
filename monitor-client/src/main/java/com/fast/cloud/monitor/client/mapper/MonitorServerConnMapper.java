package com.fast.cloud.monitor.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fast.cloud.monitor.client.entity.MonitorServerConn;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MonitorServerConnMapper extends BaseMapper<MonitorServerConn> {

    @Select("select distinct ip,url from monitor_server_conn group by ip,url")
    List<MonitorServerConn> listServer();

}
