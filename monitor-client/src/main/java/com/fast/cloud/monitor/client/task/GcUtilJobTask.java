package com.fast.cloud.monitor.client.task;

import com.fast.cloud.monitor.client.constant.MonitorConstant;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzTriggerInitialize;
import com.fast.cloud.monitor.client.service.MonitorGcService;
import com.fast.cloud.monitor.client.service.MonitorServerConnService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/10/10
 * @Version: V1.0
 */
@QuartzJob(jobKeyName = "gcUtilJob", jobKeyGroup = "gc")
@QuartzTriggerInitialize(triggerKeyName = "gcUtilJob", triggerKeyGroup = "gc", cronExpress = "0 */1 * * * ?")
public class GcUtilJobTask extends AbstractGcJobTask {
    @Override
    protected void doExecuteInternal(JobExecutionContext context, MonitorJob scheduleJob) throws JobExecutionException {
        pullGcInfo(MonitorConstant.GcType.GC_UTIL.getType());
    }

    @Autowired
    @Override
    protected void initService(
            MonitorGcService monitorGcService,
            MonitorServerConnService monitorServerConnService,
            @Qualifier("monitorPoolExecutor") ThreadPoolExecutor threadPoolExecutor) {
        super.monitorServerConnService = monitorServerConnService;
        super.monitorGcService = monitorGcService;
        super.threadPoolExecutor = threadPoolExecutor;
    }
}