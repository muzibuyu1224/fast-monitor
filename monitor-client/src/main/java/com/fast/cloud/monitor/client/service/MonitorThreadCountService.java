package com.fast.cloud.monitor.client.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.cloud.monitor.client.entity.MonitorThreadCount;
import com.fast.cloud.monitor.client.mapper.MonitorThreadCountMapper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MonitorThreadCountService extends ServiceImpl<MonitorThreadCountMapper, MonitorThreadCount> {

    /**
     * 根据名称获取记录
     *
     * @param name
     * @return
     */
    public List<MonitorThreadCount> findAllByName(String name) {
        LambdaQueryWrapper<MonitorThreadCount> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MonitorThreadCount::getApplicationName, name);
        return baseMapper.selectList(wrapper);
    }

    /**
     * 写入线程相关信息
     *
     * @return
     */
    @Override
    public boolean save(MonitorThreadCount entity) {
        return baseMapper.insert(entity) > 0;
    }

    @Async
    public void truncate() {
        baseMapper.truncate();
    }
}
