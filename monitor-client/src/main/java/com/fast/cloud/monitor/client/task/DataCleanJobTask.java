package com.fast.cloud.monitor.client.task;

import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.job.MonitorAbstractJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzTriggerInitialize;
import com.fast.cloud.monitor.client.service.MonitorClassLoadService;
import com.fast.cloud.monitor.client.service.MonitorGcService;
import com.fast.cloud.monitor.client.service.MonitorThreadCountService;
import com.fast.cloud.monitor.client.service.MonitorThreadFullService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/10/10
 * @Version: V1.0
 */
@QuartzJob(jobKeyName = "dataCleanJob数据清除", jobKeyGroup = "refresh")
@QuartzTriggerInitialize(triggerKeyName = "dataCleanJob数据清除",triggerKeyGroup = "refresh",cronExpress = "0 0 0 * * ?")
public class DataCleanJobTask extends MonitorAbstractJob {

    @Autowired
    private MonitorThreadFullService monitorThreadFullService;
    @Autowired
    private MonitorGcService monitorGcService;
    @Autowired
    private MonitorThreadCountService monitorThreadCountService;
    @Autowired
    private MonitorClassLoadService monitorClassLoadService;

    @Override
    protected void doExecuteInternal(JobExecutionContext context, MonitorJob scheduleJob) throws JobExecutionException {
        monitorThreadFullService.truncate();
        monitorGcService.truncate();
        monitorThreadCountService.truncate();
        monitorClassLoadService.truncate();
    }
}
