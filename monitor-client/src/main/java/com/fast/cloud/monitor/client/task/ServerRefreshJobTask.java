package com.fast.cloud.monitor.client.task;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.cloud.nacos.discovery.NacosDiscoveryClient;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.entity.MonitorServerConn;
import com.fast.cloud.monitor.client.job.MonitorAbstractJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzTriggerInitialize;
import com.fast.cloud.monitor.client.rest.RestClient;
import com.fast.cloud.monitor.client.service.MonitorServerConnService;
import com.google.common.collect.Lists;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.context.annotation.Lazy;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/10/10
 * @Version: V1.0
 */
@QuartzJob(jobKeyName = "serverRefreshJob服务列表更新", jobKeyGroup = "refresh")
@QuartzTriggerInitialize(triggerKeyName = "serverRefreshJob服务列表更新", triggerKeyGroup = "refresh", cronExpress = "0 */6 * * * ?")
public class ServerRefreshJobTask extends MonitorAbstractJob implements InitializingBean {

    @Lazy
    @Resource
    private NacosDiscoveryClient nacosDiscoveryClient;

    @Autowired
    private MonitorServerConnService monitorServerConnService;

    /**
     * 要准确点的话这里要加分布式锁
     *
     * @param context
     * @param scheduleJob
     * @throws JobExecutionException
     */
    @Override
    protected void doExecuteInternal(JobExecutionContext context, MonitorJob scheduleJob) throws JobExecutionException {

        /**
         * 服务检查 不一定都在nacos上面 所有在这里刷新 需要的话可以每个服务提供一个返回pid的接口
         */
        checkServerList();

        /**
         * 服务刷新 一次性根据nacos服务刷新服务状态也行
         */
        refreshServerList();
    }


    /**
     * 检车服务器
     */
    private void checkServerList() {
        List<MonitorServerConn> conns = monitorServerConnService.listAllServer();
        for (MonitorServerConn c : conns) {
            boolean b = RestClient.checkAddressReachable(c.getIp(), Integer.parseInt(c.getPort()), 100);
            c.setServerStatus(b ? "up" : "down");
        }
        monitorServerConnService.updateBatchById(conns);
    }

    /**
     * refreshServerList
     */
    private void refreshServerList() {
        // 获取nacos上面所有 服务列表 对比本地数据库 ip，更新pid
        List<String> services = nacosDiscoveryClient.getServices();
        List<ServiceInstance> result = Lists.newArrayList();
        for (String id : services) {
            result.addAll(nacosDiscoveryClient.getInstances(id));
        }

        // server list
        Map<String, ServiceInstance> instanceMap = result
                .stream()
                .collect(Collectors.toMap(x -> x.getHost() + ":" + x.getPort(), Function.identity()));

        // 获取所有up的服务 根据ip和端口对比  真是ip和端口
        List<MonitorServerConn> monitorServerConns = monitorServerConnService.listServerConn();
        int size = monitorServerConns.size();
        for (int i = size - 1; i >= 0; i--) {
            MonitorServerConn conn = monitorServerConns.get(i);
            ServiceInstance serviceInstance = instanceMap.get(conn.getIp() + ":" + conn.getPort());
            if (null == serviceInstance) {
                monitorServerConns.remove(i);
                continue;
            }
            // 获取元数据判断pid是否相同
            Map<String, String> metadata = serviceInstance.getMetadata();
            String pid = metadata.get("pid");
            if (StrUtil.equals(conn.getPid(), pid)) {
                monitorServerConns.remove(i);
                continue;
            }
            conn.setPid(pid);
        }

        // 更新conn
        if (CollUtil.isNotEmpty(monitorServerConns)) {
            monitorServerConnService.updateBatchById(monitorServerConns);
        }
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        checkServerList();
        refreshServerList();
    }
}