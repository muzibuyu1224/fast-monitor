package com.fast.cloud.monitor.client.task;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.fast.cloud.framework.core.model.FastResult;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.entity.MonitorServerConn;
import com.fast.cloud.monitor.client.entity.MonitorThreadFull;
import com.fast.cloud.monitor.client.job.MonitorAbstractJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzTriggerInitialize;
import com.fast.cloud.monitor.client.service.MonitorServerConnService;
import com.fast.cloud.monitor.client.service.MonitorThreadFullService;
import com.fast.cloud.monitor.client.task.runnable.CallableValue;
import com.fast.cloud.monitor.client.task.runnable.MonitorJobRunnable;
import com.fast.cloud.monitor.constant.MonitorUrl;
import com.google.common.collect.Maps;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * jvm数据收集定时任务
 *
 * @author muzibuyu1224
 */
@QuartzJob(jobKeyName = "fullThreadJob高内存线程查看", jobKeyGroup = "thread")
@QuartzTriggerInitialize(triggerKeyName = "fullThreadJob高内存线程查看",triggerKeyGroup = "thread",cronExpress = "")
public class FullThreadJobTask extends MonitorAbstractJob {

    @Autowired
    private MonitorThreadFullService monitorThreadFullService;
    @Autowired
    private MonitorServerConnService monitorServerConnService;
    @Autowired
    private ThreadPoolExecutor threadPoolExecutor;

    /**
     * 获取系统配置，查询服务器信息，超过百分比才查询
     * 线程加载信息收集
     */
    public void pullFullThreadInfo() {
        List<MonitorServerConn> list = monitorServerConnService.listServerConn();
        List<Future<CallableValue<FastResult<MonitorThreadFull>>>> taskList = new ArrayList<>(list.size());
        for (MonitorServerConn conn : list) {
            String url = conn.getUrl() + MonitorUrl.JAVA_HIGH_CPU_STACK;
            Map<String, Object> param = Maps.newHashMap();
            param.put("pid", conn.getPid());
            MonitorJobRunnable<FastResult<MonitorThreadFull>> monitorJobRunnable = new MonitorJobRunnable(url, param, conn,FastResult.class);
            Future<CallableValue<FastResult<MonitorThreadFull>>> future = threadPoolExecutor.submit(monitorJobRunnable);
            taskList.add(future);
        }

        // 处理结果
        List<MonitorThreadFull> httpResult = new ArrayList<>(taskList.size());
        for (Future<CallableValue<FastResult<MonitorThreadFull>>> f : taskList) {
            try {
                CallableValue<FastResult<MonitorThreadFull>> mapCallableValue = f.get(5000L, TimeUnit.MILLISECONDS);
                MonitorThreadFull monitorGc = JSON.parseObject(JSON.toJSONString(mapCallableValue.getData().getResult()), MonitorThreadFull.class);
                MonitorServerConn conn = mapCallableValue.getConn();
                monitorGc.setApplicationName(conn.getApplicationName() + ":" + conn.getPort());
                monitorGc.setIp(conn.getIp());
                monitorGc.setId(null);
                httpResult.add(monitorGc);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (CollectionUtil.isEmpty(httpResult)) {
            return;
        }
        //插入记录
        monitorThreadFullService.saveBatch(httpResult);
    }

    /**
     * 用做检查服务器cpu占用检查，设置一个百分比，如果某一时刻超出了百分比，
     * 就发送请求获取服务器状态数据，最多联系发送20次 然后关闭请求
     */
    public void cpuFullCheck() {
    }

    @Override
    protected void doExecuteInternal(JobExecutionContext context, MonitorJob scheduleJob) throws JobExecutionException {
        //pullFullThreadInfo();
    }
}
