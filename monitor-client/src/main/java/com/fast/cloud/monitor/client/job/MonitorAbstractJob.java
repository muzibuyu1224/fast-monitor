package com.fast.cloud.monitor.client.job;

import com.alibaba.fastjson.JSON;
import com.fast.cloud.framework.core.context.SpringContextHolder;
import com.fast.cloud.monitor.client.constant.MonitorConstant;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.entity.MonitorJobLog;
import com.fast.cloud.monitor.client.service.MonitorJobLogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.time.LocalDateTime;


/**
 * 定时任务
 *
 * @author muzibuyu1224
 */
@Slf4j
public abstract class MonitorAbstractJob extends QuartzJobBean {

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        before(context);
        // 获取job参数
        String jsonParam = context.getMergedJobDataMap().getString(MonitorConstant.JOB_PARAM_KEY);
        MonitorJob scheduleJob = JSON.parseObject(jsonParam, MonitorJob.class);

        //获取scheduleJobLogService
        MonitorJobLogService monitorJobLogService = SpringContextHolder.getBean(MonitorJobLogService.class);

        //数据库保存执行记录
        MonitorJobLog jobLog = new MonitorJobLog();
        jobLog.setJobId(scheduleJob.getId());
        jobLog.setScheduleName(scheduleJob.getSchedulerName());
        jobLog.setJobName(scheduleJob.getJobName());
        jobLog.setJobGroup(scheduleJob.getJobGroup());
        jobLog.setParams(scheduleJob.getParams());
        jobLog.setCreateTime(LocalDateTime.now());

        //任务开始时间
        long startTime = System.currentTimeMillis();
        try {
            //执行任务
            log.info("任务准备执行，任务ID：{}", scheduleJob.getId());
            doExecuteInternal(context, scheduleJob);
            //任务执行总时长
            long times = System.currentTimeMillis() - startTime;
            jobLog.setTimes((int) times);
            //任务状态    0：成功    1：失败
            jobLog.setStatus(0);
            log.info("任务执行完毕，任务ID：{}  总共耗时：{}毫秒", scheduleJob.getId(), times);
            after(context, scheduleJob);
        } catch (Exception e) {
            log.error("任务执行失败，任务ID：{},{}", scheduleJob.getId(), e);
            //任务执行总时长
            long times = System.currentTimeMillis() - startTime;
            jobLog.setTimes((int) times);
            //任务状态    0：成功    1：失败
            jobLog.setStatus(1);
            jobLog.setError(StringUtils.substring(e.toString(), 0, 2000));
        } finally {
            last(context, scheduleJob, jobLog);
            monitorJobLogService.save(jobLog);
        }
    }


    /**
     * 逻辑方法
     *
     * @param context
     * @param scheduleJob
     * @throws JobExecutionException
     */
    protected abstract void doExecuteInternal(JobExecutionContext context, MonitorJob scheduleJob) throws JobExecutionException;


    /**
     * 前置方法
     *
     * @param context
     */
    protected void before(JobExecutionContext context) {
    }

    /**
     * 后置方法
     *
     * @param context
     * @param scheduleJob
     */
    protected void after(JobExecutionContext context, MonitorJob scheduleJob) {
    }

    /**
     * 最终方法
     *
     * @param args
     */
    protected void last(Object... args) {
    }
}
