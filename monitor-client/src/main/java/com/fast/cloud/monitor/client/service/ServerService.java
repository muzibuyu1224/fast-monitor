package com.fast.cloud.monitor.client.service;

import com.alibaba.fastjson.JSON;
import com.fast.cloud.framework.core.model.FastResult;
import com.fast.cloud.monitor.client.entity.MonitorServerConn;
import com.fast.cloud.monitor.client.task.runnable.CallableValue;
import com.fast.cloud.monitor.client.task.runnable.MonitorJobRunnable;
import com.fast.cloud.monitor.client.vo.ServerInfoVo;
import com.fast.cloud.monitor.constant.MonitorUrl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/10/12
 * @Version: V1.0
 */
@Slf4j
@Service("serverService")
public class ServerService {

    @Autowired
    private MonitorServerConnService monitorServerConnService;

    @Autowired
    private ThreadPoolExecutor monitorPoolExecutor;

    /**
     * 获取服务器信息
     *
     * @return
     */
    public List<ServerInfoVo> listServerInfo() {
        List<MonitorServerConn> list = monitorServerConnService.listServer();
        List<Future<CallableValue<FastResult<ServerInfoVo>>>> taskList = new ArrayList<>(list.size());
        for (MonitorServerConn conn : list) {
            String url = conn.getUrl() + MonitorUrl.TINFO_SYSTEM_INFO;
            MonitorJobRunnable<FastResult<ServerInfoVo>> monitorJobRunnable = new MonitorJobRunnable(url, null, conn, FastResult.class);
            Future<CallableValue<FastResult<ServerInfoVo>>> future = monitorPoolExecutor.submit(monitorJobRunnable);
            taskList.add(future);
        }

        // 处理结果
        List<ServerInfoVo> httpResult = new ArrayList<>(taskList.size());
        for (Future<CallableValue<FastResult<ServerInfoVo>>> f : taskList) {
            try {
                CallableValue<FastResult<ServerInfoVo>> mapCallableValue = f.get(3000L, TimeUnit.MILLISECONDS);
                ServerInfoVo serverInfoVo = JSON.parseObject(JSON.toJSONString(mapCallableValue.getData().getResult()), ServerInfoVo.class);
                MonitorServerConn conn = mapCallableValue.getConn();
                serverInfoVo.setIp(conn.getIp());
                httpResult.add(serverInfoVo);
            } catch (Exception e) {
                log.error("请求异常:{}", e.getMessage());
            }
        }
        return httpResult;
    }
}
