package com.fast.cloud.monitor.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fast.cloud.monitor.client.entity.MonitorClassLoad;
import org.apache.ibatis.annotations.Delete;


public interface MonitorClassLoadMapper extends BaseMapper<MonitorClassLoad> {
    @Delete("truncate table monitor_class_load")
    void truncate();
}
