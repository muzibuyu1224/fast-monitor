package com.fast.cloud.monitor.client.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 定时任务日志
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("monitor_job_log")
public class MonitorJobLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 任务id
     */
    @TableField("job_id")
    private Long jobId;

    /**
     * 任务名称
     */
    @TableField("job_name")
    private String jobName;

    /**
     * 任务分组
     */
    @TableField("job_group")
    private String jobGroup;

    /**
     * spring bean名称
     */
    @TableField("schedule_name")
    private String scheduleName;

    /**
     * 参数
     */
    @TableField("params")
    private String params;

    /**
     * 任务状态    0：成功    1：失败
     */
    @TableField("status")
    private Integer status;

    /**
     * 失败信息
     */
    @TableField("error")
    private String error;

    /**
     * 耗时(单位：毫秒)
     */
    @TableField("times")
    private Integer times;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
}
