package com.fast.cloud.monitor.client.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 监控应用表
 */
@Data
public class MonitorServerConnVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 应用名称
     */
    private String applicationName;

    /**
     * 访问域名 带端口
     */
    private String url;

    /**
     * pid 没有就取接口中的
     */
    private String pid;

    /**
     * ip
     */
    private String ip;

    /**
     * 服务状态 up  down
     */
    private Integer serverStatus;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
}
