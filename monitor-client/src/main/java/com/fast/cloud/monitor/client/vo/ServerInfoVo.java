package com.fast.cloud.monitor.client.vo;


import com.fast.cloud.monitor.util.BigDecimalUtil;
import com.fast.cloud.monitor.util.IPUtils;
import com.fast.cloud.monitor.vo.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.CentralProcessor.TickType;
import oshi.hardware.GlobalMemory;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.FileSystem;
import oshi.software.os.OSFileStore;
import oshi.software.os.OperatingSystem;
import oshi.util.Util;

import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * 服务器相关信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServerInfoVo implements Serializable {

    private static final int OSHI_WAIT_SECOND = 1000;

    /**
     * CPU相关信息
     */
    private CpuVo cpu = new CpuVo();

    /**
     * 內存相关信息
     */
    private MemoryVo mem = new MemoryVo();

    /**
     * JVM相关信息
     */
    private JvmVo jvm = new JvmVo();

    /**
     * 服务器相关信息
     */
    private SysVo sys = new SysVo();

    /**
     * 磁盘相关信息
     */
    private List<SysFileVo> sysFiles = new LinkedList<SysFileVo>();

    private String ip;
}
