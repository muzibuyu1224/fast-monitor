package com.fast.cloud.monitor.client.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.cloud.monitor.client.dto.MonitorThreadFullDto;
import com.fast.cloud.monitor.client.entity.MonitorThreadFull;
import com.fast.cloud.monitor.client.mapper.MonitorThreadFullMapper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;


/**
 * 高cpu任务
 */
@Service
public class MonitorThreadFullService extends ServiceImpl<MonitorThreadFullMapper, MonitorThreadFull> {
    /**
     * 删除
     *
     * @param fullDto
     */
    public void delete(MonitorThreadFullDto fullDto) {
        LambdaQueryWrapper<MonitorThreadFull> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(StrUtil.isNotEmpty(fullDto.getPid()), MonitorThreadFull::getPid, fullDto.getPid())
                .eq(StrUtil.isNotEmpty(fullDto.getAppName()), MonitorThreadFull::getApplicationName, fullDto.getAppName())
                .eq(StrUtil.isNotEmpty(fullDto.getThreadName()), MonitorThreadFull::getThreadName, fullDto.getThreadName())
                .eq(StrUtil.isNotEmpty(fullDto.getIp()), MonitorThreadFull::getIp, fullDto.getIp());
        baseMapper.delete(wrapper);
    }

    /**
     * 分页查询
     *
     * @param fullDto
     * @return
     */
    public IPage<MonitorThreadFull> page(MonitorThreadFullDto fullDto) {
        LambdaQueryWrapper<MonitorThreadFull> wrapper = Wrappers.lambdaQuery();
        Page<MonitorThreadFull> page = new Page<>(fullDto.getPageNumber(), fullDto.getPageSize());
        wrapper.eq(StrUtil.isNotEmpty(fullDto.getPid()), MonitorThreadFull::getPid, fullDto.getPid())
                .eq(StrUtil.isNotEmpty(fullDto.getAppName()), MonitorThreadFull::getApplicationName, fullDto.getAppName())
                .eq(StrUtil.isNotEmpty(fullDto.getThreadName()), MonitorThreadFull::getThreadName, fullDto.getThreadName());
        if (null != fullDto.getStartTime() || null != fullDto.getEndTime()) {
            wrapper.between(MonitorThreadFull::getCreateTime, fullDto.getStartTime(), fullDto.getEndTime());
        }
        return baseMapper.selectPage(page, wrapper);
    }

    @Async
    public void truncate() {
        baseMapper.truncate();
    }
}
