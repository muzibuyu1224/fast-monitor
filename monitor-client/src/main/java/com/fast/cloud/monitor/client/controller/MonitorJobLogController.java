package com.fast.cloud.monitor.client.controller;

import com.fast.cloud.framework.core.model.FastResult;
import com.fast.cloud.framework.data.pagedata.PageData;
import com.fast.cloud.monitor.client.service.MonitorJobLogService;
import com.fast.cloud.monitor.client.vo.MonitorJobLogVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/job-log")
public class MonitorJobLogController {

    @Resource
    private MonitorJobLogService monitorJobLogService;

    @PostMapping("/page")
    public FastResult<PageData<MonitorJobLogVo>> page(@RequestBody MonitorJobLogVo monitorJobLogVo) {
        //查询列表数据
        PageData<MonitorJobLogVo> page = monitorJobLogService.page(monitorJobLogVo);
        return FastResult.build(page);
    }

    @GetMapping("/info/{id}")
    public FastResult<MonitorJobLogVo> info(@PathVariable("id") Long id) {
        MonitorJobLogVo log = monitorJobLogService.getMonitorJobLog(id);
        return FastResult.build(log);
    }
}
