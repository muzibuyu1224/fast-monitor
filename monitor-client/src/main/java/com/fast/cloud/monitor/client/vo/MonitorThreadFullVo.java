package com.fast.cloud.monitor.client.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 高cpu线程信息线程信息
 */
@Data
public class MonitorThreadFullVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String pid;

    private String nid;

    private String applicationName;

    private String threadName;

    private String context;

    private LocalDateTime dateTime;

    private String ip;

    /**
     * 端口
     */
    private String port;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
}
