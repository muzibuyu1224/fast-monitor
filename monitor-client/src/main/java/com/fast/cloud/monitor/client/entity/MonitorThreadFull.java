package com.fast.cloud.monitor.client.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 高cpu线程信息线程信息
 *
 * @author admin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("monitor_thread_full_cpu")
public class MonitorThreadFull implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @TableField("pid")
    private String pid;

    @TableField("ip")
    private String ip;

    @TableField("port")
    private String port;

    @TableField("application_name")
    private String applicationName;

    /**
     * 线程nid
     */
    @TableField("thread_nid")
    private String threadNid;

    /**
     * 线程pid
     */
    @TableField("thread_pid")
    private String threadPid;

    /**
     * 线程名称
     */
    @TableField("thread_name")
    private String threadName;

    /**
     * 线程异常内容
     */
    @TableField("thread_context")
    private String threadContext;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
}
