package com.fast.cloud.monitor.client.task.runnable;

import com.fast.cloud.monitor.client.entity.MonitorServerConn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/10/9
 * @Version: V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CallableValue <T>{
    private T data;
    private MonitorServerConn conn;
}
