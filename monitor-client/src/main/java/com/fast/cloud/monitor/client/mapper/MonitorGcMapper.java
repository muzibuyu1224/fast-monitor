package com.fast.cloud.monitor.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fast.cloud.monitor.client.entity.MonitorGc;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;

public interface MonitorGcMapper extends BaseMapper<MonitorGc> {
    @Delete("truncate table monitor_gc")
    void truncate();
}
