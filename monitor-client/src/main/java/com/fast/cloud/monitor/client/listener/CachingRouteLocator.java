//package com.fast.cloud.monitor.client.core;
//
//import org.springframework.context.ApplicationEventPublisher;
//import org.springframework.context.ApplicationEventPublisherAware;
//import org.springframework.core.Ordered;
//import org.springframework.core.annotation.AnnotationAwareOrderComparator;
//
//import java.util.concurrent.ConcurrentHashMap;
//
///** 网关监听
// * @Description:
// * @Author: muzibuyu1224
// * @Date: 2021/9/23
// * @Version: V1.0
// */
//public class CachingRouteLocator implements Ordered, RouteLocator,
//        ApplicationListener<RefreshRoutesEvent>, ApplicationEventPublisherAware {
//
//    private static final Log log = LogFactory.getLog(CachingRouteLocator.class);
//
//    private static final String CACHE_KEY = "routes";
//
//    private final RouteLocator delegate;
//
//    private final Flux<Route> routes;
//
//    private final Map<String, List> cache = new ConcurrentHashMap<>();
//
//    private ApplicationEventPublisher applicationEventPublisher;
//
//    public CachingRouteLocator(RouteLocator delegate) {
//        this.delegate = delegate;
//        routes = CacheFlux.lookup(cache, CACHE_KEY, Route.class)
//                .onCacheMissResume(this::fetch);
//    }
//
//    private Flux<Route> fetch() {
//        return this.delegate.getRoutes().sort(AnnotationAwareOrderComparator.INSTANCE);
//    }
//
//    @Override
//    public Flux<Route> getRoutes() {
//        return this.routes;
//    }
//
//    /**
//     * Clears the routes cache.
//     * @return routes flux
//     */
//    public Flux<Route> refresh() {
//        this.cache.clear();
//        return this.routes;
//    }
//
//    @Override
//    public void onApplicationEvent(RefreshRoutesEvent event) {
//        try {
//            fetch().collect(Collectors.toList()).subscribe(list -> Flux.fromIterable(list)
//                    .materialize().collect(Collectors.toList()).subscribe(signals -> {
//                        applicationEventPublisher
//                                .publishEvent(new RefreshRoutesResultEvent(this));
//                        cache.put(CACHE_KEY, signals);
//                    }, throwable -> handleRefreshError(throwable)));
//        }
//        catch (Throwable e) {
//            handleRefreshError(e);
//        }
//    }
//
//    private void handleRefreshError(Throwable throwable) {
//        if (log.isErrorEnabled()) {
//            log.error("Refresh routes error !!!", throwable);
//        }
//        applicationEventPublisher
//                .publishEvent(new RefreshRoutesResultEvent(this, throwable));
//    }
//
//    @Deprecated
//        /* for testing */ void handleRefresh() {
//        refresh();
//    }
//
//    @Override
//    public int getOrder() {
//        return 0;
//    }
//
//    @Override
//    public void setApplicationEventPublisher(
//            ApplicationEventPublisher applicationEventPublisher) {
//        this.applicationEventPublisher = applicationEventPublisher;
//    }
//
//}
