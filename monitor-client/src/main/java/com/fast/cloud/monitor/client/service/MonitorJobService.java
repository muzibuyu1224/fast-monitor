package com.fast.cloud.monitor.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.cloud.framework.data.pagedata.PageData;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.vo.MonitorJobVo;

import java.util.List;


/**
 * @Description: 定时任务
 * @Author: qinxuewu
 * @CreateDate: 26/11/2018 下午 3:36
 * @Email 870439570@qq.com
 **/
public interface MonitorJobService extends IService<MonitorJob> {

    /**
     * 获取所有job
     *
     * @return
     */
    List<MonitorJob> queryAllJobs();

    /**
     * 分页查询job
     *
     * @param monitorJobVo
     * @return
     */
    PageData<MonitorJobVo> page(MonitorJobVo monitorJobVo);


    /**
     * 执行job
     *
     * @param jobId
     */
    void run(Long jobId);

    /**
     * 更新job
     *
     * @param jobVo
     */
    void updateJob(MonitorJobVo jobVo);


    /**
     * 停止job
     *
     * @param jobId
     */
    void pause(Long jobId);

    /**
     * 恢复job
     *
     * @param jobId
     */
    void resume(Long jobId);

    MonitorJobVo getMonitorJobById(Long id);

    Long insertJob(MonitorJobVo scheduleJobVo);

    void removeJob(Long id);
}
