package com.fast.cloud.monitor.client.task;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.fast.cloud.framework.core.model.FastResult;
import com.fast.cloud.monitor.client.entity.MonitorGc;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.entity.MonitorServerConn;
import com.fast.cloud.monitor.client.job.MonitorAbstractJob;
import com.fast.cloud.monitor.client.service.MonitorGcService;
import com.fast.cloud.monitor.client.service.MonitorServerConnService;
import com.fast.cloud.monitor.client.task.runnable.CallableValue;
import com.fast.cloud.monitor.client.task.runnable.MonitorJobRunnable;
import com.fast.cloud.monitor.constant.MonitorUrl;
import com.google.common.collect.Maps;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 不使用参数来更改job逻辑而是而是创建的一堆job，方便job的初始化为和维护
 * jvm数据收集定时任务
 *
 * @author muzibuyu1224
 */
public abstract class AbstractGcJobTask extends MonitorAbstractJob {
    protected MonitorGcService monitorGcService;
    protected MonitorServerConnService monitorServerConnService;
    protected ThreadPoolExecutor threadPoolExecutor;
    protected abstract void initService(MonitorGcService monitorGcService,MonitorServerConnService monitorServerConnService,ThreadPoolExecutor threadPoolExecutor);

    /**
     * 获取受监控的数据
     * jvm gc和内存收信息收集
     */
    public void pullGcInfo(String type) {
        List<MonitorServerConn> list = monitorServerConnService.listServerConn();
        List<Future<CallableValue<FastResult<Map<String, Object>>>>> taskList = new ArrayList<>(list.size());
        for (MonitorServerConn conn : list) {
            String url = conn.getUrl() + MonitorUrl.TINFO_GC + type;
            Map<String, Object> param = Maps.newHashMap();
            param.put("pid", conn.getPid());
            MonitorJobRunnable<FastResult<Map<String,Object>>> monitorJobRunnable = new MonitorJobRunnable(url, param, conn,FastResult.class);
            Future<CallableValue<FastResult<Map<String, Object>>>> future = threadPoolExecutor.submit(monitorJobRunnable);
            taskList.add(future);
        }

        // 处理结果
        List<MonitorGc> httpResult = new ArrayList<>(taskList.size());
        for (Future<CallableValue<FastResult<Map<String, Object>>>> f : taskList) {
            try {
                CallableValue<FastResult<Map<String, Object>>> mapCallableValue = f.get(6000L, TimeUnit.MILLISECONDS);
                MonitorGc monitorGc = JSON.parseObject(JSON.toJSONString(mapCallableValue.getData().getResult()), MonitorGc.class);
                MonitorServerConn conn = mapCallableValue.getConn();
                monitorGc.setIp(conn.getIp());
                monitorGc.setGcType(type);
                monitorGc.setApplicationName(conn.getApplicationName() + ":" + conn.getPort());
                monitorGc.setId(null);
                httpResult.add(monitorGc);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (CollectionUtil.isEmpty(httpResult)) {
            return;
        }
        //插入记录
        monitorGcService.saveBatch(httpResult);
    }

    @Override
    protected abstract void doExecuteInternal(JobExecutionContext context, MonitorJob scheduleJob) throws JobExecutionException;
}
