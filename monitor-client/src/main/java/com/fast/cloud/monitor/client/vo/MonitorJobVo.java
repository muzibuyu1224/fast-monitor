package com.fast.cloud.monitor.client.vo;

import com.fast.cloud.framework.core.base.PageCondition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 定时任务
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MonitorJobVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 任务id
     */
    private Long id;

    /**
     * 参数
     */
    private String params;

    /**
     * cron表达式
     */
    private String cronExpression;

    /**
     * 任务状态  0：正常  1：暂停
     */
    private Integer status;

    /**
     * schedulerName
     */
    private String schedulerName;

    /**
     * job名称
     */
    private String jobName;
    /**
     * job所在组名称
     */
    private String jobGroup;
    /**
     * job的描述信息
     */
    private String jobDesc;
    /**
     * 与job相关联的触发器名称
     */
    private String triggerName;
    /**
     * 与job相关联的触发器所在组名称
     */
    private String triggerGroup;
    /**
     * 触发器的描述信息
     */
    private String triggerDesc;
    /**
     * job的上一次执行时间
     */
    private Long prevFireTime;
    /**
     * job的下一次执行时间
     */
    private Long nextFireTime;

    /**
     * 触发器的状态：NONE, NORMAL, PAUSED, COMPLETE, ERROR, BLOCKED
     */
    private String triggerState;


    private PageCondition pageCondition;
}
