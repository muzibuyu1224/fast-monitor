package com.fast.cloud.monitor.client.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.cloud.monitor.client.constant.MonitorConstant;
import com.fast.cloud.monitor.client.entity.MonitorServerConn;
import com.fast.cloud.monitor.client.mapper.MonitorServerConnMapper;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MonitorServerConnService extends ServiceImpl<MonitorServerConnMapper, MonitorServerConn> {

    /**
     * 获取server服务
     *
     * @return
     */
    public List<MonitorServerConn> listServerConn() {
        LambdaQueryWrapper<MonitorServerConn> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MonitorServerConn::getServerStatus, MonitorConstant.SERVER_UP);
        return baseMapper.selectList(wrapper);
    }

    /**
     * 获取同ip的所有服务
     *
     * @return
     */
    public List<MonitorServerConn> listServer() {
        return baseMapper.listServer();
    }

    /**
     * 获取所有服务
     * @return
     */
    public List<MonitorServerConn> listAllServer() {
        LambdaQueryWrapper<MonitorServerConn> wrapper = Wrappers.lambdaQuery();
        wrapper.select(MonitorServerConn::getId, MonitorServerConn::getPort, MonitorServerConn::getIp);
        return baseMapper.selectList(wrapper);
    }
}
