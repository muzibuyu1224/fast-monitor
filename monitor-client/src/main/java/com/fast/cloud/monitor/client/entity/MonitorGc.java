package com.fast.cloud.monitor.client.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 堆内存信息 GC信息
 */
@Data
@TableName("monitor_gc")
public class MonitorGc implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    /**
     * 当前应用名称
     */
    @TableField("application_name")
    private String applicationName;

    @TableField("ip")
    private String ip;

    /**
     * 端口
     */
    @TableField("port")
    private String port;

    /**
     * gc
     * gcCapacity
     * gcMetaCapacity
     * gcNew
     * gcOld
     * gcNewCapacity
     * gcOldCapacity
     * gcutil
     */
    @TableField("gc_type")
    private String gcType;

    /**
     * Survivor0空间的大小。
     */
    @TableField("s0c")
    private String S0C;
    /**
     * Survivor1空间的大小。
     */
    @TableField("s1c")
    private String S1C;
    /**
     * Survivor0已用空间的大小。
     */
    @TableField("s0u")
    private String S0U;
    /**
     * Survivor1已用空间的大小。
     */
    @TableField("s1u")
    private String S1U;
    /**
     * Eden空间的大小。
     */
    @TableField("ec")
    private String EC;
    /**
     * Eden已用空间的大小。
     */
    @TableField("eu")
    private String EU;
    /**
     * 老年代空间的大小。
     */
    @TableField("oc")
    private String OC;
    /**
     * 老年代已用空间的大小。
     */
    @TableField("ou")
    private String OU;
    /**
     * 元空间的大小（Metaspace）
     */
    @TableField("mc")
    private String MC;
    /**
     * 元空间已使用大小
     */
    @TableField("mu")
    private String MU;
    /**
     * 压缩类空间大小（compressed class space）
     */
    @TableField("ccsc")
    private String CCSC;
    /**
     * 压缩类空间已使用大小
     */
    @TableField("ccsu")
    private String CCSU;
    /**
     * 新生代gc次数
     */
    @TableField("ygc")
    private String YGC;
    /**
     * 新生代gc耗时（秒）
     */
    @TableField("ygct")
    private String YGCT;
    /**
     * Full gc次数
     */
    @TableField("fgc")
    private String FGC;
    /**
     * Full gc耗时（秒）
     */
    @TableField("fgct")
    private String FGCT;
    /**
     * gc总耗时（秒）
     */
    @TableField("gct")
    private String GCT;



    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;


}
