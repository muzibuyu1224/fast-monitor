package com.fast.cloud.monitor.client.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 线程信息
 *
 * @author admin
 */
@Data
@TableName("monitor_thread_count")
public class MonitorThreadCount implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @TableField("ip")
    private String ip;

    @TableField("pid")
    private String pid;

    /**
     * 端口
     */
    @TableField("port")
    private String port;

    /**
     * 当前应用名称
     */
    @TableField("application_name")
    private String applicationName;
    /**
     * 总线程数
     */
    @TableField("total")
    private int total;

    /**
     * 运行中的线程
     */
    @TableField("runnable")
    private int runnable;

    /**
     * 这个状态就是有限的(时间限制)的WAITING, 一般出现在调用wait(long), join(long)等情况下, 另外一个线程sleep后
     * 休眠的线程数
     */
    @TableField("timed_waiting")
    private int timedWaiting;

    /**
     * 这个状态下是指线程拥有了某个锁之后, 调用了他的wait方法, 等待的线程数
     */
    @TableField("waiting")
    private int waiting;


    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
}
