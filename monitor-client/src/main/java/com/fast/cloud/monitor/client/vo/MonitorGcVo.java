package com.fast.cloud.monitor.client.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 堆内存信息 GC信息
 */
@Data
public class MonitorGcVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 当前应用名称
     */
    private String applicationName;
    /**
     * 时间 格式 MM/dd HH:mm"
     */
    private String date;

    /**
     * Survivor0空间的大小。
     */
    private String S0C;
    /**
     * Survivor1空间的大小。
     */
    private String S1C;
    /**
     * Survivor0已用空间的大小。
     */
    private String S0U;
    /**
     * Survivor1已用空间的大小。
     */
    private String S1U;
    /**
     * Eden空间的大小。
     */
    private String EC;
    /**
     * Eden已用空间的大小。
     */
    private String EU;
    /**
     * 老年代空间的大小。
     */
    private String OC;
    /**
     * 老年代已用空间的大小。
     */
    private String OU;
    /**
     * 元空间的大小（Metaspace）
     */
    private String MC;
    /**
     * 元空间已使用大小
     */
    private String MU;
    /**
     * 压缩类空间大小（compressed class space）
     */
    private String CCSC;
    /**
     * 压缩类空间已使用大小
     */
    private String CCSU;
    /**
     * 新生代gc次数
     */
    private String YGC;
    /**
     * 新生代gc耗时（秒）
     */
    private String YGCT;
    /**
     * Full gc次数
     */
    private String FGC;
    /**
     * Full gc耗时（秒）
     */
    private String FGCT;
    /**
     * gc总耗时（秒）
     */
    private String GCT;

    private String ip;

    /**
     * 端口
     */
    private String port;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
}
