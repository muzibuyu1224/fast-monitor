package com.fast.cloud.monitor.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fast.cloud.monitor.client.entity.MonitorThreadCount;
import org.apache.ibatis.annotations.Delete;


public interface MonitorThreadCountMapper extends BaseMapper<MonitorThreadCount> {

    @Delete("truncate table monitor_thread_count")
    void truncate();
}
