package com.fast.cloud.monitor.client.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.cloud.framework.data.pagedata.PageData;
import com.fast.cloud.monitor.client.assember.MonitorConvert;
import com.fast.cloud.monitor.client.entity.MonitorJobLog;
import com.fast.cloud.monitor.client.mapper.MonitorJobLogMapper;
import com.fast.cloud.monitor.client.service.MonitorJobLogService;
import com.fast.cloud.monitor.client.vo.MonitorJobLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("monitorJobLogServiceImpl")
public class MonitorJobLogServiceImpl extends ServiceImpl<MonitorJobLogMapper, MonitorJobLog> implements MonitorJobLogService {

    @Autowired
    private MonitorConvert monitorConvert;

    @Override
    public PageData<MonitorJobLogVo> page(MonitorJobLogVo monitorJobLogVo) {
        IPage<MonitorJobLog> page = new Page<>(monitorJobLogVo.getPageCondition().getCurrentPage(), monitorJobLogVo.getPageCondition().getPageSize());
        LambdaQueryWrapper<MonitorJobLog> wrapper = Wrappers.lambdaQuery();
        wrapper
                .eq(ObjectUtil.isNotNull(monitorJobLogVo.getJobId()), MonitorJobLog::getJobId, monitorJobLogVo.getJobId())
                .like(StrUtil.isNotEmpty(monitorJobLogVo.getJobName()), MonitorJobLog::getJobName, monitorJobLogVo.getJobName())
                .like(StrUtil.isNotEmpty(monitorJobLogVo.getJobGroup()), MonitorJobLog::getJobGroup, monitorJobLogVo.getJobGroup());
        IPage<MonitorJobLog> selectPage = baseMapper.selectPage(page, wrapper);
        List<MonitorJobLogVo> monitorJobLogVos = monitorConvert.toMonitorJobLogList(selectPage.getRecords());
        return PageData.buildPageData(selectPage, monitorJobLogVos);
    }

    @Override
    public MonitorJobLogVo getMonitorJobLog(Long id) {
        MonitorJobLog monitorJobLog = baseMapper.selectById(id);
        return monitorConvert.monitorJobLogToVo(monitorJobLog);
    }
}
