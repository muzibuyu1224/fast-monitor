package com.fast.cloud.monitor.client.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.cloud.monitor.client.entity.MonitorClassLoad;
import com.fast.cloud.monitor.client.mapper.MonitorClassLoadMapper;
import com.fast.cloud.monitor.util.JvmFormatUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MonitorClassLoadService extends ServiceImpl<MonitorClassLoadMapper, MonitorClassLoad> {

    /**
     * @param name
     * @return
     */
    public List<MonitorClassLoad> findAllByName(String name) {
        LambdaQueryWrapper<MonitorClassLoad> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MonitorClassLoad::getApplicationName, name);
        return baseMapper.selectList(wrapper);
    }

    /**
     * 写入类加载信息
     */
    @Override
    public boolean save(MonitorClassLoad entity) {
        return baseMapper.insert(entity) > 0;
    }

    @Async
    public void truncate() {
        baseMapper.truncate();
    }
}
