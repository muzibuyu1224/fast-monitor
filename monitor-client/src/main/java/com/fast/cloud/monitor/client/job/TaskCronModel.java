package com.fast.cloud.monitor.client.job;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description: 定时任务表达式构建实体
 * @Author: muzibuyu1224
 * @Date: 2021/10/10
 * @Version: V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskCronModel {
    /**
     * 所选作业类型:
     * 1  -> 每天
     * 2  -> 每月
     * 3  -> 每周
     * 4  ->间隔（每隔2个小时，每隔30分钟）
     * 5  -> 只执行一次
     */
    Integer jobType;

    /**
     * 一周的哪几天
     */
    Integer[] dayOfWeeks;

    /**
     * 一个月的哪几天
     */
    Integer[] dayOfMonths;

    /**
     * 秒
     */
    String second;

    /**
     * 分
     */
    String minute;

    /**
     * 时
     */
    String hour;
}
