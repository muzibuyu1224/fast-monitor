package com.fast.cloud.monitor.client.task;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.fast.cloud.framework.core.model.FastResult;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.entity.MonitorServerConn;
import com.fast.cloud.monitor.client.entity.MonitorThreadCount;
import com.fast.cloud.monitor.client.job.MonitorAbstractJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzTriggerInitialize;
import com.fast.cloud.monitor.client.service.MonitorServerConnService;
import com.fast.cloud.monitor.client.service.MonitorThreadCountService;
import com.fast.cloud.monitor.client.task.runnable.CallableValue;
import com.fast.cloud.monitor.client.task.runnable.MonitorJobRunnable;
import com.fast.cloud.monitor.constant.MonitorUrl;
import com.google.common.collect.Maps;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * jvm数据收集定时任务
 */
@QuartzJob(jobKeyName = "threadJob线程统计", jobKeyGroup = "thread")
@QuartzTriggerInitialize(triggerKeyName = "threadJob线程统计",triggerKeyGroup = "thread",cronExpress = "0 0/1 12 * * ?")
public class ThreadJobTask extends MonitorAbstractJob {

    @Autowired
    private MonitorThreadCountService monitorThreadCountService;
    @Autowired
    private MonitorServerConnService monitorServerConnService;
    @Autowired
    private ThreadPoolExecutor threadPoolExecutor;

    /**
     * 线程加载信息收集
     */
    public void pullThreadInfo() {
        List<MonitorServerConn> list = monitorServerConnService.listServerConn();
        List<Future<CallableValue<FastResult<MonitorThreadCount>>>> taskList = new ArrayList<>(list.size());
        for (MonitorServerConn conn : list) {
            String url = conn.getUrl() + MonitorUrl.TINFO_THREAD;
            Map<String, Object> param = Maps.newHashMap();
            param.put("pid", conn.getPid());
            MonitorJobRunnable<FastResult<MonitorThreadCount>> monitorJobRunnable = new MonitorJobRunnable(url, param, conn,FastResult.class);
            Future<CallableValue<FastResult<MonitorThreadCount>>> future = threadPoolExecutor.submit(monitorJobRunnable);
            taskList.add(future);
        }

        // 处理结果
        List<MonitorThreadCount> httpResult = new ArrayList<>(taskList.size());
        for (Future<CallableValue<FastResult<MonitorThreadCount>>> f : taskList) {
            try {
                CallableValue<FastResult<MonitorThreadCount>> mapCallableValue = f.get(5000L, TimeUnit.MILLISECONDS);
                MonitorThreadCount threadCount = JSON.parseObject(JSON.toJSONString(mapCallableValue.getData().getResult()), MonitorThreadCount.class);
                MonitorServerConn conn = mapCallableValue.getConn();
                threadCount.setIp(conn.getIp());
                threadCount.setApplicationName(conn.getApplicationName() + ":" + conn.getPort());
                threadCount.setId(null);
                httpResult.add(threadCount);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (CollectionUtil.isEmpty(httpResult)) {
            return;
        }
        //插入记录
        monitorThreadCountService.saveBatch(httpResult);
    }

    /**
     * 用做检查服务器cpu占用检查，设置一个百分比，如果某一时刻超出了百分比，
     * 就发送请求获取服务器状态数据，最多联系发送20次 然后关闭请求
     * 创建定时任务的时候创建十个时间点
     */
    public void cpuFullCheck() {

        // 获取
        List<MonitorServerConn> list = monitorServerConnService.listServerConn();


        //
    }

    @Override
    protected void doExecuteInternal(JobExecutionContext context, MonitorJob scheduleJob) throws JobExecutionException {
        pullThreadInfo();
    }
}
