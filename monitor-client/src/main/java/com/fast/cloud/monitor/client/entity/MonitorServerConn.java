package com.fast.cloud.monitor.client.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 监控应用表
 */
@Data
@TableName("monitor_server_conn")
public class MonitorServerConn implements Serializable {

    private static final long serialVersionUID = 1L;


    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 应用名称 服务名称
     */
    @TableField("application_name")
    private String applicationName;

    /**
     * 访问域名 带端口
     */
    @TableField("url")
    private String url;


    /**
     * pid 没有就取接口中的
     */
    @TableField("pid")
    private String pid;

    /**
     * ip
     */
    @TableField("ip")
    private String ip;

    /**
     * ip
     */
    @TableField("port")
    private String port;

    /**
     * 服务状态 up  down
     */
    @TableField("server_status")
    private String serverStatus;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
}
