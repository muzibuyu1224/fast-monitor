package com.fast.cloud.monitor.client.job.annotation;

import com.fast.cloud.monitor.client.job.core.QuartzJobBeanPostProcessor;
import org.quartz.*;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 用于定义quartz job
 * {@link QuartzJobBeanPostProcessor}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Component
public @interface QuartzJob {

    /**
     * job key
     */
    String jobKeyName() default "";

    /**
     * job group
     */
    String jobKeyGroup() default Scheduler.DEFAULT_GROUP;

    /**
     * description
     */
    String jobDescription() default "";

    /**
     * Whether or not the <code>Job</code> should remain stored after it is
     * orphaned (no <code>{@link Trigger}s</code> point to it).
     *
     * @see JobDetail#isDurable()
     */
    boolean storeDurably() default true;


    Class<? extends Job> jobClass() default Job.class;

    /**
     * usingJobData json
     * @return
     */
    String jobParameter() default "{}";

    boolean isDurable() default false;

    boolean isPersistJobDataAfterExecution() default false;

    boolean isConcurrentExectionDisallowed() default false;

    boolean requestsRecovery() default false;
}
