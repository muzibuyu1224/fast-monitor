package com.fast.cloud.monitor.client.job.annotation;

import org.quartz.Scheduler;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 用于定义quartz job's trigger
 * 方便起见，用在初始化的时候使用
 * 系统中配置job不覆盖 不然的话每次重启会按照这个注解配置的优先
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Component
public @interface QuartzTriggerInitialize {

    /**
     * the name of Trigger's TriggerKey
     */
    String triggerKeyName() default "";

    /**
     * the group of Trigger's TriggerKey
     */
    String triggerKeyGroup() default Scheduler.DEFAULT_GROUP;

    /**
     * trigger's  description
     */
    String triggerDescription() default "";

    /**
     * the cron expression string to base the schedule on
     */
    String cronExpress();
}
