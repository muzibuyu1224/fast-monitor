package com.fast.cloud.monitor.client.job.annotation;

import com.fast.cloud.monitor.client.job.core.QuartzExtendConfig;
import com.fast.cloud.monitor.client.job.core.QuartzJobRegistrar;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启动对quartz job的扫描，并自动注册到quartz scheduler
 *
 * @see QuartzJobRegistrar
 * @see QuartzJobBeanPostProcessor
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import({QuartzJobRegistrar.class, QuartzExtendConfig.class})
public @interface QuartzJobScan {
    /**
     * Base packages to scan for quartz job
     */
    String[] packages() default {};

    String PACKAGE = "packages";
}
