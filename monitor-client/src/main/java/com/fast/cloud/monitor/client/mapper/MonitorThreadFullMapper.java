package com.fast.cloud.monitor.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fast.cloud.monitor.client.entity.MonitorThreadFull;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Update;


public interface MonitorThreadFullMapper extends BaseMapper<MonitorThreadFull> {
    @Delete("truncate table monitor_thread_full_cpu")
    void truncate();
}
