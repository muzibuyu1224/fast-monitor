package com.fast.cloud.monitor.client.job.core;

import com.fast.cloud.monitor.client.job.MonitorAbstractJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzJob;
import com.fast.cloud.monitor.client.job.annotation.QuartzTriggerInitialize;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.spi.JobFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.quartz.SchedulerAccessor;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 处理{@link SchedulerFactoryBean}的BeanPostProcessor，
 * 存在{@link QuartzJob}注解的类都会被添加到QuartzScheduler中，
 * 如果类上同时存在{@link QuartzTriggerInitialize}，也会创建一个trigger，关联到job上。
 */
@Slf4j
@Component
public class QuartzJobBeanPostProcessor implements BeanPostProcessor, ApplicationContextAware {

    private ApplicationContext applicationContext;

    /**
     * 只加载已经写好了cron表达式的数据
     * @param bean
     * @param beanName
     * @return
     * @throws BeansException
     */
    @Override
    @Nullable
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (!(bean instanceof SchedulerFactoryBean)) {
            return bean;
        }
        try {
            SchedulerFactoryBean schedulerFactoryBean = (SchedulerFactoryBean) bean;
            // 如果jobFactory为空，则给它赋值
            setJobFactoryIfNull(schedulerFactoryBean);

            Class<SchedulerAccessor> schedulerAccessorClass = SchedulerAccessor.class;
            // 获取已有的JobDetail
            List<JobDetail> jobDetails = getExistingJobDetails(schedulerAccessorClass, schedulerFactoryBean);
            // 获取已有的Trigger
            List<Trigger> triggers = getExistingTriggers(schedulerAccessorClass, schedulerFactoryBean);

            String[] jobBeanNames = applicationContext.getBeanNamesForType(MonitorAbstractJob.class);

            /**
             * 遍历job
             */
            for (String jobBeanName : jobBeanNames) {
                @SuppressWarnings("unchecked")
                Class<? extends Job> jobType = (Class<? extends Job>) applicationContext.getType(jobBeanName);

                // 排除类中类中多次扫描
                if (jobType == null||jobBeanName.contains("$")) {
                    continue;
                }
                QuartzJob quartzJob = AnnotationUtils.findAnnotation(jobType, QuartzJob.class);
                QuartzTriggerInitialize quartzTriggerInitialize = AnnotationUtils.findAnnotation(jobType, QuartzTriggerInitialize.class);
                if (quartzJob != null) {
                    // job key
                    String jobKeyName = quartzJob.jobKeyName();

                    // TODO json param 指定参数
                    //String jsonParam = quartzJob.jobParameter();
                    // 创建JobDetail并添加到列表中 执行的job固定一个job
                        JobDetail jobDetail = JobBuilder.newJob(jobType)
                                .withIdentity(StringUtils.hasText(jobKeyName) ? jobKeyName : jobBeanName, quartzJob.jobKeyGroup())
                                .withDescription(quartzJob.jobDescription())
                                .storeDurably(quartzJob.storeDurably())
                                //.usingJobData(MonitorConstant.JOB_PARAM_KEY, jsonParam)
                                .build();
                    jobDetails.add(jobDetail);
                    // job触发器
                    if (quartzTriggerInitialize != null) {
                        // 创建job对应的trigger并添加到列表中
                        String triggerKeyName = quartzTriggerInitialize.triggerKeyName();

                        // 执行表达式 如果没有的 页面添加时候再绑定
                        String cronExpress = quartzTriggerInitialize.cronExpress();

                        if (StringUtils.hasText(cronExpress)) {
                            if (!StringUtils.hasText(triggerKeyName)) {
                                triggerKeyName = jobBeanName + "Trigger";
                            }

                            // 表达式调度构建器(即任务执行的时间)
                            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpress);

                            // 按新的cronExpression表达式构建一个新的trigger
                            CronTrigger trigger = TriggerBuilder.newTrigger()
                                    .forJob(jobDetail.getKey())
                                    .withIdentity(triggerKeyName, quartzTriggerInitialize.triggerKeyGroup())
                                    .withDescription(quartzTriggerInitialize.triggerDescription())
                                    .withSchedule(scheduleBuilder)
                                    .build();
                            triggers.add(trigger);
                        } else {
                            log.warn("未指定cronExpress，不会创建job相关联的trigger");
                        }
                    }
                }
            }

            // 重新设置JobDetails和Triggers
            schedulerFactoryBean.setJobDetails(jobDetails.toArray(new JobDetail[0]));
            schedulerFactoryBean.setTriggers(triggers.toArray(new Trigger[0]));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return bean;
    }

    /**
     * 创建jobFactory工厂
     * 如果{@link SchedulerFactoryBean}为null，
     * 则将其设置为{@link SpringBeanJobFactory}
     */
    private void setJobFactoryIfNull(SchedulerFactoryBean schedulerFactoryBean) {
        try {
            Class<SchedulerFactoryBean> clazz = SchedulerFactoryBean.class;
            Field field = clazz.getDeclaredField("jobFactory");
            field.setAccessible(true);
            JobFactory jobFactory = (JobFactory) field.get(schedulerFactoryBean);
            if (jobFactory == null) {
                schedulerFactoryBean.setJobFactory(applicationContext.getBean(SpringBeanJobFactory.class));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 获取已经存在的job详情
     * @param schedulerAccessorClass
     * @param schedulerFactoryBean
     * @return
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    private List<JobDetail> getExistingJobDetails(Class<SchedulerAccessor> schedulerAccessorClass,
                                                  SchedulerFactoryBean schedulerFactoryBean)
            throws NoSuchFieldException, IllegalAccessException {
        Field f1 = schedulerAccessorClass.getDeclaredField("jobDetails");
        f1.setAccessible(true);
        @SuppressWarnings("unchecked")
        List<JobDetail> jobDetails = (List<JobDetail>) f1.get(schedulerFactoryBean);
        if (jobDetails == null) {
            jobDetails = new ArrayList<>();
        } else {
            jobDetails = new ArrayList<>(jobDetails);
        }
        return jobDetails;
    }

    /**
     * 获取存在的triggers
     * @param schedulerAccessorClass
     * @param schedulerFactoryBean
     * @return
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    private List<Trigger> getExistingTriggers(Class<SchedulerAccessor> schedulerAccessorClass,
                                              SchedulerFactoryBean schedulerFactoryBean)
            throws NoSuchFieldException, IllegalAccessException {
        Field f2 = schedulerAccessorClass.getDeclaredField("triggers");
        f2.setAccessible(true);
        @SuppressWarnings("unchecked")
        List<Trigger> triggers = (List<Trigger>) f2.get(schedulerFactoryBean);
        if (triggers == null) {
            triggers = new ArrayList<>();
        } else {
            triggers = new ArrayList<>(triggers);
        }
        return triggers;
    }

    @Override
    @Nullable
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
