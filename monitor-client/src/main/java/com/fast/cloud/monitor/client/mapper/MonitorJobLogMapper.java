package com.fast.cloud.monitor.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fast.cloud.monitor.client.entity.MonitorJobLog;


public interface MonitorJobLogMapper extends BaseMapper<MonitorJobLog> {
}
