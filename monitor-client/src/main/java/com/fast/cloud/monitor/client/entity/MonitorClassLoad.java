package com.fast.cloud.monitor.client.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * 类加载信息
 *
 * @author admin
 */

@Data
@TableName("monitor_class_load")
public class MonitorClassLoad implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 当前应用进程ID
     */
    @TableField("pid")
    private Integer pid;

    @TableField("ip")
    private String ip;

    /**
     * 端口
     */
    @TableField("port")
    private String port;

    /**
     * 当前应用名称  配置文件 spring.application.name=xxxx
     */
    @TableField("application_name")
    private String applicationName;

    /**
     * 表示载入了类的数量
     */
    @TableField("loaded")
    private String loaded;

    /**
     * 表示载入了类的合计
     */
    @TableField("loaded_bytes")
    private String loadedBytes;

    /**
     * 表示卸载类的数量
     */
    @TableField("unloaded")
    private String unloaded;

    /**
     * 表示卸载类合计大小
     */
    @TableField("unloaded_bytes")
    private String unloadedBytes;
    /**
     * 表示加载和卸载类总共的耗时
     */
    @TableField("loaded_time")
    private String loadedTime;
    /**
     * 表示编译任务执行的次数
     */
    @TableField("compiled")
    private String compiled;
    /**
     * 表示编译失败的次数
     */
    @TableField("failed")
    private String failed;
    /**
     * 表示编译不可用的次数
     */
    @TableField("invalid")
    private String invalid;

    /**
     * 表示编译的总耗时
     */
    @TableField("upLoad_time")
    private String upLoadTime;


    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

}
