package com.fast.cloud.monitor.client.rest;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/9/19
 * @Version: V1.0
 */
@Slf4j
public class RestClient {

    /**
     * get
     *
     * @param url
     * @param params
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T get(String url, Map<String, Object> params, Class<T> clazz) {
        if (null == params) {
            params = new HashMap<>();
        }
        String reqUrl = url += ("?" + asUrlVariables(params));
        return RestTemplateEnum.SINGLE_INSTANCE.getRestTemplate().getForObject(reqUrl, clazz);
    }

    /**
     * postJson
     *
     * @param reqUrl
     * @param jsonParam
     * @return
     */
    public static String postJson(String reqUrl, String jsonParam) {
        //设置 Header
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        //设置参数
        HttpEntity<String> requestEntity = new HttpEntity<>(jsonParam, httpHeaders);
        //执行请求
        ResponseEntity<String> resp = RestTemplateEnum.SINGLE_INSTANCE.getRestTemplate()
                .exchange(reqUrl, HttpMethod.POST, requestEntity, String.class);
        //返回请求返回值
        return resp.getBody();
    }


    /**
     * postForm
     *
     * @param reqUrl
     * @param formParam
     * @return
     */
    public static <T> T postForm(String reqUrl, Map<String, Object> formParam, Class<T> clazz) {
        //设置 Header
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        //设置参数
        HttpEntity<Object> requestEntity = new HttpEntity<>(formParam, httpHeaders);
        //执行请求
        ResponseEntity<T> resp = RestTemplateEnum.SINGLE_INSTANCE.getRestTemplate()
                .exchange(reqUrl, HttpMethod.POST, requestEntity, clazz);
        //返回请求返回值
        return resp.getBody();
    }


    /**
     * 封装参数
     *
     * @param source
     * @return
     */
    public static String asUrlVariables(Map<String, Object> source) {
        Iterator<String> it = source.keySet().iterator();
        StringBuilder urlVariables = new StringBuilder();
        while (it.hasNext()) {
            String key = it.next();
            String value = "";
            Object object = source.get(key);
            if (object != null) {
                if (!StrUtil.isEmpty(object.toString())) {
                    value = object.toString();
                }
            }
            urlVariables.append("&").append(key).append("=").append(value);
        }
        String string = urlVariables.toString();
        // 去掉第一个&
        return string.startsWith("&") ? string.substring(1) : string;
    }

    /**
     * @param url
     * @param updateMap
     * @param returnClass
     * @param resource
     * @param <T>
     * @return
     */
    public static <T> T upload(String url, Map<String, Object> updateMap, Class<T> returnClass, Resource resource) {
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        parts.add("file", resource);
        for (Map.Entry<String, Object> entry : updateMap.entrySet()) {
            Object value = entry.getValue();
            String key = entry.getKey();
            parts.add(key, value);
        }
        HttpEntity<Object> httpEntity = new HttpEntity<>(parts, headers);
        ResponseEntity<T> exchange = RestTemplateEnum.SINGLE_INSTANCE.getRestTemplate().exchange(url, HttpMethod.POST, httpEntity, returnClass);
        return exchange.getBody();
    }


    /**
     * socket 检查目标端口和ip是否可用
     *
     * @param address
     * @param port
     * @param timeout
     */
    public static boolean checkAddressReachable(String address, int port, int timeout) {
        Socket socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(address, port), timeout);
            return true;
        } catch (IOException exception) {
            log.info("Address {}:{} is not in use", address, port);
            return false;
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                ;
            }
        }
    }

}