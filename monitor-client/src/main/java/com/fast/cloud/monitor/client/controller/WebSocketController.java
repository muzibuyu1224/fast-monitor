package com.fast.cloud.monitor.client.controller;

import com.fast.cloud.framework.core.model.FastResult;
import com.fast.cloud.monitor.client.entity.MonitorClassLoad;
import com.fast.cloud.monitor.client.entity.MonitorGc;
import com.fast.cloud.monitor.client.entity.MonitorThreadCount;
import com.fast.cloud.monitor.client.rest.RestClient;
import com.fast.cloud.monitor.client.service.MonitorClassLoadService;
import com.fast.cloud.monitor.client.service.MonitorGcService;
import com.fast.cloud.monitor.client.service.MonitorThreadCountService;
import com.fast.cloud.monitor.constant.MonitorUrl;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;


@Controller
public class WebSocketController {

    @Autowired
    private MonitorGcService monitorGcService;
    @Autowired
    private MonitorClassLoadService monitorClassLoadService;
    @Autowired
    private MonitorThreadCountService monitorThreadCountService;

    /**
     * gc 内存信息
     * MessageMapping注解和我们之前使用的@RequestMapping类似  前端主动发送消息到后台时的地址
     * SendTo注解表示当服务器有消息需要推送的时候，会对订阅了@SendTo中路径的浏览器发送消息。
     *
     * @return
     */
    @MessageMapping("/gc")
    @SendTo("/topic/gc")
    public List<MonitorGc> socketGc(String name) {
        return monitorGcService.findAllByName(name);
    }

    /**
     * 类加载相关信息
     *
     * @param name
     * @return
     */
    @MessageMapping("/cl")
    @SendTo("/topic/cl")
    public List<MonitorClassLoad> socketCl(String name) {
        return monitorClassLoadService.findAllByName(name);
    }

    /**
     * 线程相关信息
     *
     * @param name
     * @return
     */
    @MessageMapping("/thread")
    @SendTo("/topic/thread")
    public List<MonitorThreadCount> socketThread(String name) {
        return monitorThreadCountService.findAllByName(name);
    }

    /**
     * 日志
     *
     * @return
     */
    @MessageMapping("/weblog")
    @SendTo("/topic/weblog")
    public String weblog(String url) {
        Map<String, Object> param = Maps.newHashMap();
        param.put("pid", "");
        FastResult<String> logred = RestClient.get(url + MonitorUrl.TINFO_LOG_READER, param, FastResult.class);
        return logred.getResult();
    }
}
