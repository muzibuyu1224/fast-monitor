package com.fast.cloud.monitor.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/10/9
 * @Version: V1.0
 */
@Configuration
public class ExecutorConfig {

    /**
     * 线程池
     *
     * @return
     */
    @Bean
    public ThreadPoolExecutor monitorPoolExecutor() {
        return new ThreadPoolExecutor(
                // 核心线程池大小为1
                Runtime.getRuntime().availableProcessors(),
                // 最大线程池大小为2*Ncpu
                Runtime.getRuntime().availableProcessors() * 2+1,
                60,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(200),
                new NameTreadFactory());
    }

    static class NameTreadFactory implements ThreadFactory {
        private final AtomicInteger mThreadNum = new AtomicInteger(1);

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, "Monitor-Job-Thread-" + mThreadNum.getAndIncrement());
        }
    }
}
