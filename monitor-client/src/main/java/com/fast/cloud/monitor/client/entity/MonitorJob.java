package com.fast.cloud.monitor.client.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 定时任务
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("monitor_job")
public class MonitorJob implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 任务id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 参数
     */
    @TableField("params")
    private String params;

    /**
     * cron表达式
     */
    @TableField("cron_expression")
    private String cronExpression;

    /**
     * 任务状态  0：正常  1：暂停
     */
    @TableField("status")
    private Integer status;

    /**
     * schedulerName
     */
    @TableField("scheduler_name")
    private String schedulerName;

    /**
     * job名称
     */
    @TableField("job_name")
    private String jobName;
    /**
     * job所在组名称
     */
    @TableField("job_group")
    private String jobGroup;
    /**
     * job的描述信息
     */
    @TableField("job_desc")
    private String jobDesc;
    /**
     * 与job相关联的触发器名称
     */
    @TableField("trigger_name")
    private String triggerName;
    /**
     * 与job相关联的触发器所在组名称
     */
    @TableField("trigger_group")
    private String triggerGroup;
    /**
     * 触发器的描述信息
     */
    @TableField("trigger_desc")
    private String triggerDesc;

    /**
     * job的上一次执行时间
     */
    @TableField("prev_fire_time")
    private Long prevFireTime;
    /**
     * job的下一次执行时间
     */
    @TableField("next_fire_time")
    private Long nextFireTime;

    /**
     * 触发器的状态：NONE, NORMAL, PAUSED, COMPLETE, ERROR, BLOCKED
     */
    @TableField("trigger_state")
    private String triggerState;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
}
