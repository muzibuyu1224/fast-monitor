package com.fast.cloud.monitor.client.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.cloud.monitor.client.entity.MonitorGc;
import com.fast.cloud.monitor.client.mapper.MonitorGcMapper;
import com.fast.cloud.monitor.util.JvmFormatUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MonitorGcService extends ServiceImpl<MonitorGcMapper, MonitorGc> {

    /**
     * gc信息
     */
    @Override
    public boolean save(MonitorGc entity) {
        return baseMapper.insert(entity) > 0;
    }

    /**
     * 根据名称查询
     *
     * @param name
     * @return
     */
    public List<MonitorGc> findAllByName(String name) {
        LambdaQueryWrapper<MonitorGc> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(MonitorGc::getApplicationName, name);
        return baseMapper.selectList(wrapper);
    }

    @Async
    public void clearAll() {
        baseMapper.delete(null);
    }

    @Async
    public void truncate() {
        baseMapper.truncate();
    }
}
