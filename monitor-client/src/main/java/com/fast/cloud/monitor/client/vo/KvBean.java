package com.fast.cloud.monitor.client.vo;

import lombok.Data;

@Data
public class KvBean {
    private String key;
    private String value;

}
