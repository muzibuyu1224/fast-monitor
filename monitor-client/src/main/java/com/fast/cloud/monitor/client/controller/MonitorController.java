package com.fast.cloud.monitor.client.controller;

import com.fast.cloud.framework.core.model.FastResult;
import com.fast.cloud.framework.data.pagedata.PageData;
import com.fast.cloud.monitor.client.service.MonitorJobService;
import com.fast.cloud.monitor.client.service.ServerService;
import com.fast.cloud.monitor.client.vo.MonitorJobVo;
import com.fast.cloud.monitor.client.vo.ServerInfoVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/server")
public class MonitorController {

    @Resource
    private ServerService serverService;



    @GetMapping("list-server")
    public FastResult<List<ServerInfoVo>> listServerInfo(){
        return FastResult.build(serverService.listServerInfo());
    }
}
