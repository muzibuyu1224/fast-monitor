package com.fast.cloud.monitor.client.job;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.fast.cloud.monitor.client.constant.MonitorConstant;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.utils.Constant;
import org.quartz.*;


/**
 * 定时任务工具类
 *
 * @author muzibuyu1224
 */
public class QuartzJobUtils {
    /**
     * 获取触发器key
     */
    private static TriggerKey getTriggerKey(String name, String group) {
        return TriggerKey.triggerKey(name, group);
    }

    /**
     * 获取jobKey
     */
    private static JobKey getJobKey(String name, String group) {
        return JobKey.jobKey(name, group);
    }

    /**
     * 获取表达式触发器
     */
    public static CronTrigger getCronTrigger(Scheduler scheduler, String name, String group) {
        try {
            return (CronTrigger) scheduler.getTrigger(getTriggerKey(name, group));
        } catch (SchedulerException e) {
            throw new RuntimeException("getCronTrigger异常，请检查qrtz开头的表，是否有脏数据", e);
        }
    }

    /**
     * 创建定时任务
     *
     * @param scheduler
     * @param job
     * @param scheduleJob
     */
    public static void createScheduleJob(Scheduler scheduler, Class<? extends Job> job, MonitorJob scheduleJob) {
        try {
            //构建job
            JobDetail jobDetail = JobBuilder.newJob(job)
                    .withIdentity(getJobKey(scheduleJob.getJobName(), scheduleJob.getJobGroup()))
                    .withDescription(scheduleJob.getJobDesc())
                    .usingJobData(MonitorConstant.JOB_PARAM_KEY, JSON.toJSONString(scheduleJob))
                    .build();


            //构建cron
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(scheduleJob.getCronExpression())
                    .withMisfireHandlingInstructionDoNothing();

            //根据cron，构建一个CronTrigger
            CronTrigger trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity(getTriggerKey(scheduleJob.getTriggerName(), scheduleJob.getTriggerGroup()))
                    .withDescription(scheduleJob.getTriggerDesc())
                    .withSchedule(scheduleBuilder)
                    .build();
            //放入参数，运行时的方法可以获取
            scheduler.scheduleJob(jobDetail, trigger);
            //暂停任务
            if (scheduleJob.getStatus().equals(Constant.ScheduleStatus.PAUSE.getValue())) {
                pauseJob(scheduler, scheduleJob.getJobName(), scheduleJob.getJobGroup());
            }
        } catch (SchedulerException e) {
            throw new RuntimeException("创建定时任务失败", e);
        }
    }

    /**
     * 更新定时任务
     *
     * @param scheduler
     * @param scheduleJob
     */
    public static void updateScheduleJob(Scheduler scheduler, MonitorJob scheduleJob) {
        try {
            TriggerKey triggerKey = getTriggerKey(scheduleJob.getTriggerName(), scheduleJob.getTriggerGroup());
            //表达式调度构建器
            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(scheduleJob.getCronExpression())
                    .withMisfireHandlingInstructionDoNothing();

            CronTrigger trigger = getCronTrigger(scheduler, scheduleJob.getTriggerName(), scheduleJob.getTriggerGroup());
            //按新的cronExpression表达式重新构建trigger
            trigger = trigger
                    .getTriggerBuilder()
                    .withIdentity(getTriggerKey(scheduleJob.getTriggerName(), scheduleJob.getTriggerGroup()))
                    .withDescription(scheduleJob.getTriggerDesc())
                    .withSchedule(scheduleBuilder)
                    .usingJobData(MonitorConstant.JOB_PARAM_KEY, JSON.toJSONString(scheduleJob))
                    .build();
            //参数
            scheduler.rescheduleJob(triggerKey, trigger);
            //暂停任务
            if (scheduleJob.getStatus() == Constant.ScheduleStatus.PAUSE.getValue()) {
                pauseJob(scheduler, scheduleJob.getJobName(), scheduleJob.getJobGroup());
            }

        } catch (SchedulerException e) {
            throw new RuntimeException("更新定时任务失败", e);
        }
    }

    /**
     * 执行job
     *
     * @param scheduler
     * @param scheduleJob
     */
    public static void run(Scheduler scheduler, MonitorJob scheduleJob) {
        try {
            //参数
            JobDataMap dataMap = new JobDataMap();
            dataMap.put(MonitorConstant.JOB_PARAM_KEY, JSON.toJSONString(scheduleJob.getParams()));
            scheduler.triggerJob(getJobKey(scheduleJob.getJobName(), scheduleJob.getJobGroup()), dataMap);
        } catch (SchedulerException e) {
            throw new RuntimeException("立即执行定时任务失败", e);
        }
    }

    /**
     * 暂停任务
     *
     * @param scheduler
     * @param name
     * @param group
     */
    public static void pauseJob(Scheduler scheduler, String name, String group) {
        try {
            scheduler.pauseJob(getJobKey(name, group));
        } catch (SchedulerException e) {
            throw new RuntimeException("暂停定时任务失败", e);
        }
    }

    /**
     * 恢复任务
     *
     * @param scheduler
     * @param name
     * @param group
     */
    public static void resumeJob(Scheduler scheduler, String name, String group) {
        try {
            scheduler.resumeJob(getJobKey(name, group));
        } catch (SchedulerException e) {
            throw new RuntimeException("暂停定时任务失败", e);
        }
    }

    /**
     * 删除定时任务
     */
    public static void deleteScheduleJob(Scheduler scheduler, String name, String group) {
        try {
            scheduler.deleteJob(getJobKey(name, group));
        } catch (SchedulerException e) {
            throw new RuntimeException("删除定时任务失败", e);
        }
    }

    /**
     * 构建Cron表达式
     *
     * @param cronModel
     * @return
     */
    public static String createCronExpression(TaskCronModel cronModel) {
        StringBuffer cronExp = new StringBuffer("");
        if (null == cronModel.getJobType()) {
            cronModel.setJobType(1);
        }
        if (StrUtil.isEmpty(cronModel.getSecond())) {
            cronModel.setSecond("*");
        }
        if (StrUtil.isEmpty(cronModel.getMinute())) {
            cronModel.setMinute("*");
        }
        if (StrUtil.isEmpty(cronModel.getHour())) {
            cronModel.setHour("*");
        }
        //秒
        cronExp.append(cronModel.getSecond()).append(" ");
        //分
        cronExp.append(cronModel.getMinute()).append(" ");
        //小时
        cronExp.append(cronModel.getHour()).append(" ");

        //每天
        if (cronModel.getJobType() == 1) {
            //日
            cronExp.append("* ");
            //月
            cronExp.append("* ");
            //周
            cronExp.append("?");
            return cronExp.toString();
        }

        //按每周
        if (cronModel.getJobType() == 3) {
            //一个月中第几天
            cronExp.append("? ");
            //月份
            cronExp.append("* ");
            //周
            Integer[] weeks = cronModel.getDayOfWeeks();
            for (int i = 0; i < weeks.length; i++) {
                if (i == 0) {
                    cronExp.append(weeks[i]);
                } else {
                    cronExp.append(",").append(weeks[i]);
                }
            }
            return cronModel.toString();
        }

        //按每月
        if (cronModel.getJobType() == 2) {
            //一个月中的哪几天
            Integer[] days = cronModel.getDayOfMonths();
            for (int i = 0; i < days.length; i++) {
                if (i == 0) {
                    cronExp.append(days[i]);
                } else {
                    cronExp.append(",").append(days[i]);
                }
            }
            //月份
            cronExp.append(" * ");
            //周
            cronExp.append("?");
            return cronModel.toString();
        }
        return cronExp.toString();
    }

    /**
     * 生成计划的详细描述
     */
    public static String createDescription(TaskCronModel taskScheduleModel) {
        StringBuffer description = new StringBuffer("");
        if (null != taskScheduleModel.getSecond()
                && null != taskScheduleModel.getMinute()
                && null != taskScheduleModel.getHour()) {
            //按每天
            if (taskScheduleModel.getJobType() == 1) {
                description.append("每天");
                description.append(taskScheduleModel.getHour()).append("时");
                description.append(taskScheduleModel.getMinute()).append("分");
                description.append(taskScheduleModel.getSecond()).append("秒");
                description.append("执行");
            }

            //按每周
            else if (taskScheduleModel.getJobType() == 3) {
                if (taskScheduleModel.getDayOfWeeks() != null && taskScheduleModel.getDayOfWeeks().length > 0) {
                    String days = "";
                    for (int i : taskScheduleModel.getDayOfWeeks()) {
                        days += "周" + i;
                    }
                    description.append("每周的").append(days).append(" ");
                }
                if (null != taskScheduleModel.getSecond()
                        && null != taskScheduleModel.getMinute()
                        && null != taskScheduleModel.getHour()) {
                    description.append(",");
                    description.append(taskScheduleModel.getHour()).append("时");
                    description.append(taskScheduleModel.getMinute()).append("分");
                    description.append(taskScheduleModel.getSecond()).append("秒");
                }
                description.append("执行");
            }

            //按每月
            else if (taskScheduleModel.getJobType() == 2) {
                //选择月份
                if (taskScheduleModel.getDayOfMonths() != null && taskScheduleModel.getDayOfMonths().length > 0) {
                    String days = "";
                    for (int i : taskScheduleModel.getDayOfMonths()) {
                        days += i + "号";
                    }
                    description.append("每月的").append(days).append(" ");
                }
                description.append(taskScheduleModel.getHour()).append("时");
                description.append(taskScheduleModel.getMinute()).append("分");
                description.append(taskScheduleModel.getSecond()).append("秒");
                description.append("执行");
            }

        }
        return description.toString();
    }
}
