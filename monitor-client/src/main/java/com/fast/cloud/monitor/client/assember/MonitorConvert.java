package com.fast.cloud.monitor.client.assember;


import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.entity.MonitorJobLog;
import com.fast.cloud.monitor.client.vo.MonitorJobLogVo;
import com.fast.cloud.monitor.client.vo.MonitorJobVo;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author muzibuyu
 */
@Mapper(componentModel = "spring")
public interface MonitorConvert {

    MonitorJobLog voToMonitorJobLog(MonitorJobLogVo vo);

    MonitorJobLogVo monitorJobLogToVo(MonitorJobLog entity);

    List<MonitorJobLogVo> toMonitorJobLogList(List<MonitorJobLog> list);


    MonitorJob voToMonitorJob(MonitorJobVo vo);

    MonitorJobVo monitorJobToVo(MonitorJob entity);

    List<MonitorJobVo> toMonitorJobList(List<MonitorJob> list);
}
