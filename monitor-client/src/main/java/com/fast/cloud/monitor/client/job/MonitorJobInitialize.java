package com.fast.cloud.monitor.client.job;

import cn.hutool.core.util.StrUtil;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.service.MonitorJobService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.Trigger.TriggerState;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;


/**
 * job初始化
 */
@Slf4j
@Component
public class MonitorJobInitialize implements InitializingBean {

    @Autowired
    private MonitorJobService monitorJobService;
    @Autowired
    private Scheduler scheduler;

    /**
     * 根据trigger查询对应的 {@link MonitorJob}
     *
     * @param triggerName  trigger's name
     * @param triggerGroup trigger's group
     * @return trigger对应的 {@link MonitorJob}
     * @throws SchedulerException scheduler异常
     */
    MonitorJob getMonitorJobInfo(String triggerName, String triggerGroup) throws SchedulerException {
        TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroup);
        MonitorJob quartBean = initMonitorJob(triggerKey);
        Assert.notNull(quartBean, "Job not exist");
        return quartBean;
    }

    /**
     * 初始化构建monitorJob
     * @param triggerKey
     * @return
     * @throws SchedulerException
     */
    private MonitorJob initMonitorJob(TriggerKey triggerKey) throws SchedulerException {
        MonitorJob monitorJob = new MonitorJob();
        String schedulerName = scheduler.getSchedulerName();
        monitorJob.setSchedulerName(schedulerName);
        Trigger trigger = scheduler.getTrigger(triggerKey);
        TriggerState triggerState = scheduler.getTriggerState(triggerKey);
        // 设置触发器相关属性
        monitorJob.setTriggerName(triggerKey.getName());
        monitorJob.setTriggerGroup(triggerKey.getGroup());
        monitorJob.setTriggerDesc(trigger.getDescription());
        Optional.ofNullable(trigger.getPreviousFireTime()).ifPresent(t -> monitorJob.setPrevFireTime(t.getTime()));
        Optional.ofNullable(trigger.getNextFireTime()).ifPresent(t -> monitorJob.setNextFireTime(t.getTime()));
        monitorJob.setTriggerState(triggerState.name());
        monitorJob.setCronExpression(((CronTriggerImpl)trigger).getCronExpression());
        JobKey jobKey = trigger.getJobKey();
        if (jobKey != null) {
            JobDetail jobDetail = scheduler.getJobDetail(jobKey);
            // 设置job相关属性
            monitorJob.setJobName(jobDetail.getKey().getName());
            monitorJob.setJobGroup(jobDetail.getKey().getGroup());
            monitorJob.setJobDesc(jobDetail.getDescription());
            monitorJob.setStatus(0);
        }
        return monitorJob;
    }

    /**
     * 执行job
     *
     * @param jobName  job名称
     * @param jobGroup job所在组
     */
    void triggerJob(String jobName, String jobGroup) throws SchedulerException {
        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        scheduler.triggerJob(jobKey);
    }

    /**
     * 根据trigger初始化
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        List<MonitorJob> annotationJobs = new ArrayList<>();
        Set<TriggerKey> triggerKeys = scheduler.getTriggerKeys(GroupMatcher.anyTriggerGroup());
        for (TriggerKey triggerKey : triggerKeys) {
            try {
                MonitorJob monitorJob = initMonitorJob(triggerKey);
                annotationJobs.add(monitorJob);
            } catch (SchedulerException e) {
                log.warn("createQuartBean error", e);
            }
        }
        // 查询数据库所有job
        List<MonitorJob> monitorJobs = monitorJobService.queryAllJobs();
        List<MonitorJob> sameJob = monitorJobs
                .stream()
                .map(j1 -> annotationJobs
                        .stream()
                        .filter(j2 -> sameJob(j1, j2))
                        .findAny()
                        .orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        // 移除相同的 如果配置了覆盖job  这里要强行更新，不然参数id等无法更新到新的job中
        annotationJobs.removeAll(sameJob);
        if(!annotationJobs.isEmpty()){
            monitorJobService.saveBatch(annotationJobs);
            monitorJobs = monitorJobService.queryAllJobs();
        }
        // 把所有加载出来 放在内存中 其实这里把自动扫描进去ioc的逻辑去掉更好 只是做好了不想删了
        for (MonitorJob job : monitorJobs) {
            QuartzJobUtils.updateScheduleJob(scheduler, job);
        }
    }

    /**
     * sameJob
     * @param j1
     * @param j2
     * @return
     */
    private boolean sameJob(MonitorJob j1,MonitorJob j2){
        return StrUtil.equals(j1.getCronExpression(), j2.getCronExpression())
                && StrUtil.equals(j1.getJobGroup(), j2.getJobGroup())
                && StrUtil.equals(j1.getJobName(), j2.getJobName())
                && StrUtil.equals(j1.getSchedulerName(), j2.getSchedulerName())
                && StrUtil.equals(j1.getTriggerGroup(), j2.getTriggerGroup())
                && StrUtil.equals(j1.getTriggerName(), j2.getTriggerName())
                && StrUtil.equals(j1.getParams(), j2.getParams());
    }
}
