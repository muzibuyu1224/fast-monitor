package com.fast.cloud.monitor.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fast.cloud.framework.data.pagedata.PageData;
import com.fast.cloud.monitor.client.entity.MonitorJobLog;
import com.fast.cloud.monitor.client.vo.MonitorJobLogVo;


public interface MonitorJobLogService extends IService<MonitorJobLog> {

    PageData<MonitorJobLogVo> page(MonitorJobLogVo monitorJobLogVo);

    MonitorJobLogVo getMonitorJobLog(Long id);
}
