package com.fast.cloud.monitor.client.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 高cpu线程信息线程信息
 *
 * @author admin
 */
@Data
public class MonitorThreadFullDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String pid;

    private String appName;

    private String threadName;

    private String ip;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

    private int pageNumber;

    private int pageSize;
}
