package com.fast.cloud.monitor.client.listener;

import org.springframework.context.ApplicationEvent;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/9/23
 * @Version: V1.0
 */
public class ServerChangeEvent  extends ApplicationEvent {
    public ServerChangeEvent(Object source) {
        super(source);
    }
}
