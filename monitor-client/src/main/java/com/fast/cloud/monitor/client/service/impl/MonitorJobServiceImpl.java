package com.fast.cloud.monitor.client.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fast.cloud.framework.data.pagedata.PageData;
import com.fast.cloud.monitor.client.assember.MonitorConvert;
import com.fast.cloud.monitor.client.entity.MonitorJob;
import com.fast.cloud.monitor.client.job.QuartzJobUtils;
import com.fast.cloud.monitor.client.mapper.MonitorJobMapper;
import com.fast.cloud.monitor.client.service.MonitorJobService;
import com.fast.cloud.monitor.client.utils.Constant;
import com.fast.cloud.monitor.client.vo.MonitorJobVo;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service("monitorJobServiceImpl")
public class MonitorJobServiceImpl extends ServiceImpl<MonitorJobMapper, MonitorJob> implements MonitorJobService {

    @Autowired
    private Scheduler scheduler;

    @Autowired
    private MonitorConvert monitorConvert;

    /**
     * 获取所有job
     *
     * @return
     */
    @Override
    public List<MonitorJob> queryAllJobs() {
        LambdaQueryWrapper<MonitorJob> wrapper = Wrappers.lambdaQuery();
        wrapper.select(MonitorJob::getId,MonitorJob::getCronExpression, MonitorJob::getJobName, MonitorJob::getJobGroup, MonitorJob::getParams, MonitorJob::getSchedulerName, MonitorJob::getTriggerGroup, MonitorJob::getTriggerName,MonitorJob::getStatus,MonitorJob::getTriggerState);
        return baseMapper.selectList(wrapper);
    }

    @Override
    public PageData<MonitorJobVo> page(MonitorJobVo monitorJobVo) {
        IPage<MonitorJob> page = new Page<>(monitorJobVo.getPageCondition().getCurrentPage(), monitorJobVo.getPageCondition().getPageSize());
        LambdaQueryWrapper<MonitorJob> wrapper = Wrappers.lambdaQuery();
        wrapper.like(StrUtil.isNotEmpty(monitorJobVo.getJobName()), MonitorJob::getJobName, monitorJobVo.getJobName())
                .like(StrUtil.isNotEmpty(monitorJobVo.getTriggerName()), MonitorJob::getTriggerName, monitorJobVo.getTriggerName());
        IPage<MonitorJob> selectPage = baseMapper.selectPage(page, wrapper);
        List<MonitorJobVo> monitorJobLogVos = monitorConvert.toMonitorJobList(selectPage.getRecords());
        return PageData.buildPageData(selectPage, monitorJobLogVos);
    }

    @Override
    public void run(Long jobId) {
        QuartzJobUtils.run(scheduler, baseMapper.selectById(jobId));
    }

    @Override
    public void updateJob(MonitorJobVo jobVo) {
        MonitorJob job = monitorConvert.voToMonitorJob(jobVo);
        baseMapper.updateById(job);
        MonitorJob newJob = baseMapper.selectById(job.getId());
        QuartzJobUtils.updateScheduleJob(scheduler, newJob);
    }

    @Override
    public void pause(Long jobId) {
        MonitorJob monitorJob = baseMapper.selectById(jobId);
        monitorJob.setStatus(1);
        updateById(monitorJob);
        QuartzJobUtils.pauseJob(scheduler, monitorJob.getJobName(), monitorJob.getJobGroup());
        baseMapper.updateStatus(Constant.ScheduleStatus.PAUSE.getValue(), jobId);
    }

    @Override
    public void resume(Long jobId) {
        MonitorJob monitorJob = baseMapper.selectById(jobId);
        monitorJob.setStatus(0);
        updateById(monitorJob);
        QuartzJobUtils.resumeJob(scheduler, monitorJob.getJobName(), monitorJob.getJobGroup());
        baseMapper.updateStatus(Constant.ScheduleStatus.NORMAL.getValue(), jobId);
    }

    @Override
    public MonitorJobVo getMonitorJobById(Long id) {
        MonitorJob monitorJob = baseMapper.selectById(id);
        return monitorConvert.monitorJobToVo(monitorJob);
    }

    @Override
    public Long insertJob(MonitorJobVo monitorJobVo) {
        MonitorJob monitorJob = monitorConvert.voToMonitorJob(monitorJobVo);
        baseMapper.insert(monitorJob);
        // todo
        QuartzJobUtils.createScheduleJob(scheduler, null, monitorJob);
        return monitorJob.getId();
    }

    @Override
    public void removeJob(Long id) {
        MonitorJob monitorJob = baseMapper.selectById(id);
        QuartzJobUtils.deleteScheduleJob(scheduler, monitorJob.getJobName(), monitorJob.getJobGroup());
        removeById(id);
    }

}
