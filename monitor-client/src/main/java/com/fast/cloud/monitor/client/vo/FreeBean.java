package com.fast.cloud.monitor.client.vo;

import lombok.Data;

@Data
public class FreeBean {

    /**
     * 总内存
     */
    public String total;
    /**
     * 已使用内存
     */
    public String used;
    /**
     * 空闲的内存数:
     */
    public String free;
}
