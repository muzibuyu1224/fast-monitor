package com.fast.cloud.monitor.client.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 线程信息
 */
@Data
public class MonitorThreadCountVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    /**
     * 当前应用名称
     */
    private String applicationName;
    /**
     * 时间 格式 MM/dd HH:mm"
     */
    private String date;
    /**
     * 总线程数
     */
    private int total;

    /**
     * 运行中的线程
     */
    private int runnable;
    /**
     * 这个状态就是有限的(时间限制)的WAITING, 一般出现在调用wait(long), join(long)等情况下, 另外一个线程sleep后
     * 休眠的线程数
     */
    private int timedWaiting;
    /**
     * 这个状态下是指线程拥有了某个锁之后, 调用了他的wait方法, 等待的线程数
     */
    private int waiting;

    private String ip;

    /**
     * 端口
     */
    private String port;

    /**
     * 创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
}
