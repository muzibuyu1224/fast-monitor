package com.fast.cloud;

import com.fast.cloud.monitor.client.job.annotation.QuartzJobScan;
import com.fast.cloud.monitor.util.IPUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableDiscoveryClient
@SpringBootApplication
@EnableScheduling
@EnableAsync
@QuartzJobScan(packages = {"com.fast.cloud.monitor.client.task"})
@MapperScan({"com.fast.cloud.monitor.client.mapper"})
public class MonitorClientApplication {
    public static void main(String[] args) {
        IPUtils.getHostIp();
        SpringApplication.run(MonitorClientApplication.class, args);
    }
}
