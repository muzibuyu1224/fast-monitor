

DROP TABLE IF EXISTS `monitor_class_load`;
DROP TABLE IF EXISTS `monitor_gc`;
CREATE TABLE `monitor_class_load`
(
    `id`               bigint(20) NOT NULL COMMENT '主键ID',
    `ip`               VARCHAR(20) NULL COMMENT 'Ip',
    `pid`              varchar(10) NULL COMMENT 'Pid',
    `port`             varchar(10) NULL COMMENT '端口',
    `application_name` varchar(50) NOT NULL COMMENT 'App名称',
    `loaded`           varchar(10) NOT NULL COMMENT '装载数量',
    `loaded_bytes`     varchar(10) NOT NULL COMMENT '装载字节',
    `loaded_time`      varchar(20) NOT NULL COMMENT '装载时间',
    `unloaded`         varchar(10) NOT NULL COMMENT '卸载',
    `unloaded_bytes`   varchar(10) NOT NULL COMMENT '卸载字节',
    `upLoad_time`      varchar(20) NOT NULL COMMENT '卸载时间',
    `compiled`         varchar(10) NOT NULL COMMENT '完成数量',
    `failed`           varchar(10) NOT NULL COMMENT '失败数量',
    `invalid`          varchar(10) NOT NULL COMMENT '不可用数量',
    `create_time`      datetime     NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
CREATE TABLE `monitor_gc`
(
    `id`               bigint(20) NOT NULL COMMENT '主键ID',
    `ip`               VARCHAR(20) NULL COMMENT 'Ip',
    `pid`              varchar(10) NULL COMMENT 'Pid',
    `port`             varchar(10) NULL COMMENT '端口',
    `application_name` varchar(50) NOT NULL COMMENT 'App名称',
    `gc_type`          varchar(20) NOT NULL COMMENT '类型',
    `ccsc`             varchar(20) COMMENT '压缩类空间大小',
    `ccsu`             varchar(20) COMMENT '压缩类空间已使用大小',
    `ec`               varchar(20) COMMENT 'Eden空间的大小',
    `eu`               varchar(20) COMMENT 'Eden已用空间的大小',
    `fgc`              varchar(20) COMMENT 'Full gc次数',
    `fgct`             varchar(20) COMMENT 'Full gc耗时(秒)',
    `gct`              varchar(20) COMMENT 'gc总耗时(秒)',
    `mc`               varchar(20) COMMENT '元空间的大小',
    `mu`               varchar(20) COMMENT '元空间已使用大小',
    `oc`               varchar(20) COMMENT '老年代空间的大小',
    `ou`               varchar(20) COMMENT '老年代已用空间的大小',
    `s0c`              varchar(20) COMMENT 'Survivor0空间的大小',
    `s0u`              varchar(20) COMMENT 'Survivor0已用空间的大小',
    `s1c`              varchar(20) COMMENT 'Survivor1空间的大小',
    `s1u`              varchar(20) COMMENT 'Survivor1已用空间的大小',
    `ygc`              varchar(20) COMMENT '新生代gc次数',
    `ygct`             varchar(20) COMMENT '新生代gc耗时(秒)',
    `create_time`      datetime    NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `monitor_thread_count`;
CREATE TABLE `monitor_thread_count`
(
    `id`               bigint(20) NOT NULL COMMENT '主键ID',
    `ip`               VARCHAR(20) NULL COMMENT 'Ip',
    `pid`              varchar(10) NULL COMMENT 'Pid',
    `port`             varchar(10) NULL COMMENT '端口',
    `application_name` varchar(50) NOT NULL COMMENT 'App名称',
    `runnable`         int(6) NOT NULL COMMENT '运行中的线程数',
    `timed_waiting`    int(6) NOT NULL COMMENT '休眠的线程数',
    `waiting`          int(6) NOT NULL COMMENT '等待的线程数',
    `total`            int(6) NOT NULL COMMENT '总线程数',
    `create_time`      datetime    NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `monitor_thread_full_cpu`;
DROP TABLE IF EXISTS `monitor_job_log`;

CREATE TABLE `monitor_thread_full_cpu`
(
    `id`               bigint(20) NOT NULL COMMENT '主键ID',
    `ip`               VARCHAR(20) NULL COMMENT 'Ip',
    `pid`              varchar(10) NULL COMMENT 'Pid',
    `port`             varchar(10) NULL COMMENT '端口',
    `application_name` varchar(50) NOT NULL COMMENT 'App名称',
    `thread_pid`       varchar(10) NULL COMMENT '线程pid',
    `thread_nid`       varchar(30) NULL COMMENT '线程nid',
    `thread_name`      varchar(50) NULL COMMENT '线程名称',
    `thread_context`   varchar(1000) NULL COMMENT '线程异常内容',
    `create_time`      datetime    NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `monitor_job_log`
(
    `id`            bigint(20) NOT NULL COMMENT '主键ID',
    `job_id`        bigint(20) NOT NULL COMMENT '任务id',
    `job_name`      varchar(50)   DEFAULT NULL COMMENT '任务名称',
    `job_group`     varchar(50)   DEFAULT NULL COMMENT '任务分组',
    `schedule_name` varchar(50)   DEFAULT NULL COMMENT 'schedule name',
    `params`        varchar(2000) DEFAULT NULL COMMENT '参数',
    `status`        tinyint(1) NOT NULL COMMENT '任务状态    0：成功    1：失败',
    `error`         varchar(1000) DEFAULT NULL COMMENT '失败信息',
    `times`         int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
    `create_time`   datetime      DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`),
    KEY             `job_id` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务日志';

DROP TABLE IF EXISTS `monitor_job`;
CREATE TABLE `monitor_job`
(
    `id`              bigint(20) NOT NULL COMMENT '主键ID',
    `params`          varchar(2000) DEFAULT NULL COMMENT '参数',
    `cron_expression` varchar(50)   DEFAULT NULL COMMENT 'cron表达式',
    `status`          tinyint(1) DEFAULT NULL COMMENT '任务状态',
    `scheduler_name`   varchar(50)   DEFAULT NULL COMMENT 'schedule name',
    `job_name`        varchar(50)   DEFAULT NULL COMMENT 'job名称',
    `job_group`       varchar(50)   DEFAULT NULL COMMENT 'job分组',
    `job_desc`        varchar(50)   DEFAULT NULL COMMENT 'job描述',
    `trigger_name`    varchar(50)   DEFAULT NULL COMMENT '触发器名称',
    `trigger_group`   varchar(50)   DEFAULT NULL COMMENT '触发器分组',
    `trigger_desc`    varchar(50)   DEFAULT NULL COMMENT '触发器描述',
    `prev_fire_time`  bigint(20) DEFAULT NULL COMMENT '上一次执行时间',
    `next_fire_time`  bigint(20) DEFAULT NULL COMMENT '下一次执行时间',
    `trigger_state`   varchar(10)   DEFAULT NULL COMMENT '触发器状态',
    `create_time`     datetime      DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- INSERT INTO `schedule_job`
-- VALUES ('4', 'monitiorTask', 'pullGc', null, '0 0/1 * * * ?', '0', 'GC和内存收信息收集', '2018-12-14 15:39:54');
-- INSERT INTO `schedule_job`
-- VALUES ('5', 'monitiorTask', 'pullThread', null, '0 0/1 * * * ?', '0', '线程信息收集', '2018-12-14 15:40:34');
-- INSERT INTO `schedule_job`
-- VALUES ('6', 'monitiorTask', 'pullClassload', null, '0 0/1 * * * ?', '0', '类加载信息收集', '2018-12-14 15:41:15');
-- INSERT INTO `schedule_job`
-- VALUES ('7', 'monitiorTask', 'clearAll', null, '0 0 0 * * ?', '0', '每日凌晨清空JVM收集的信息', '2018-12-14 15:41:56');
--

DROP TABLE IF EXISTS `monitor_server_conn`;
CREATE TABLE `monitor_server_conn`
(
    `id`               bigint(20) NOT NULL COMMENT '主键ID',
    `ip`               varchar(20) NULL COMMENT 'Ip',
    `pid`              varchar(10) NULL COMMENT 'Pid',
    `port`             varchar(10) NULL COMMENT '端口',
    `url`              varchar(50) NULL COMMENT 'url访问地址(Ip带端口)',
    `application_name` varchar(50) NOT NULL COMMENT 'App名称',
    `server_status`    varchar(5)  NOT NULL COMMENT '服务状态',
    `create_time`      datetime DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='监控应用表';


DROP TABLE IF EXISTS `monitor_config`;
CREATE TABLE `monitor_config`
(
    `id`          bigint(20) NOT NULL COMMENT '主键ID',
    `name`        varchar(20) NULL COMMENT '配置名称',
    `code`        varchar(20) NULL COMMENT '配置标识',
    `value`       varchar(10) NULL COMMENT '配置值',
    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='监控配置表';


DROP TABLE IF EXISTS QRTZ_FIRED_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_PAUSED_TRIGGER_GRPS;
DROP TABLE IF EXISTS QRTZ_SCHEDULER_STATE;
DROP TABLE IF EXISTS QRTZ_LOCKS;
DROP TABLE IF EXISTS QRTZ_SIMPLE_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_SIMPROP_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_CRON_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_BLOB_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_TRIGGERS;
DROP TABLE IF EXISTS QRTZ_JOB_DETAILS;
DROP TABLE IF EXISTS QRTZ_CALENDARS;


CREATE TABLE QRTZ_JOB_DETAILS
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    JOB_NAME  VARCHAR(200) NOT NULL,
    JOB_GROUP VARCHAR(200) NOT NULL,
    DESCRIPTION VARCHAR(250) NULL,
    JOB_CLASS_NAME   VARCHAR(250) NOT NULL,
    IS_DURABLE VARCHAR(1) NOT NULL,
    IS_NONCONCURRENT VARCHAR(1) NOT NULL,
    IS_UPDATE_DATA VARCHAR(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR(1) NOT NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
);

CREATE TABLE QRTZ_TRIGGERS
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    JOB_NAME  VARCHAR(200) NOT NULL,
    JOB_GROUP VARCHAR(200) NOT NULL,
    DESCRIPTION VARCHAR(250) NULL,
    NEXT_FIRE_TIME BIGINT(13) NULL,
    PREV_FIRE_TIME BIGINT(13) NULL,
    PRIORITY INTEGER NULL,
    TRIGGER_STATE VARCHAR(16) NOT NULL,
    TRIGGER_TYPE VARCHAR(8) NOT NULL,
    START_TIME BIGINT(13) NOT NULL,
    END_TIME BIGINT(13) NULL,
    CALENDAR_NAME VARCHAR(200) NULL,
    MISFIRE_INSTR SMALLINT(2) NULL,
    JOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
        REFERENCES QRTZ_JOB_DETAILS(SCHED_NAME,JOB_NAME,JOB_GROUP)
);

CREATE TABLE QRTZ_SIMPLE_TRIGGERS
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    REPEAT_COUNT BIGINT(7) NOT NULL,
    REPEAT_INTERVAL BIGINT(12) NOT NULL,
    TIMES_TRIGGERED BIGINT(10) NOT NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE QRTZ_CRON_TRIGGERS
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    CRON_EXPRESSION VARCHAR(200) NOT NULL,
    TIME_ZONE_ID VARCHAR(80),
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE QRTZ_SIMPROP_TRIGGERS
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    STR_PROP_1 VARCHAR(512) NULL,
    STR_PROP_2 VARCHAR(512) NULL,
    STR_PROP_3 VARCHAR(512) NULL,
    INT_PROP_1 INT NULL,
    INT_PROP_2 INT NULL,
    LONG_PROP_1 BIGINT NULL,
    LONG_PROP_2 BIGINT NULL,
    DEC_PROP_1 NUMERIC(13,4) NULL,
    DEC_PROP_2 NUMERIC(13,4) NULL,
    BOOL_PROP_1 VARCHAR(1) NULL,
    BOOL_PROP_2 VARCHAR(1) NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
    REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE QRTZ_BLOB_TRIGGERS
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    BLOB_DATA BLOB NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);

CREATE TABLE QRTZ_CALENDARS
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    CALENDAR_NAME  VARCHAR(200) NOT NULL,
    CALENDAR BLOB NOT NULL,
    PRIMARY KEY (SCHED_NAME,CALENDAR_NAME)
);

CREATE TABLE QRTZ_PAUSED_TRIGGER_GRPS
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    TRIGGER_GROUP  VARCHAR(200) NOT NULL,
    PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP)
);

CREATE TABLE QRTZ_FIRED_TRIGGERS
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    ENTRY_ID VARCHAR(95) NOT NULL,
    TRIGGER_NAME VARCHAR(200) NOT NULL,
    TRIGGER_GROUP VARCHAR(200) NOT NULL,
    INSTANCE_NAME VARCHAR(200) NOT NULL,
    FIRED_TIME BIGINT(13) NOT NULL,
    SCHED_TIME BIGINT(13) NOT NULL,
    PRIORITY INTEGER NOT NULL,
    STATE VARCHAR(16) NOT NULL,
    JOB_NAME VARCHAR(200) NULL,
    JOB_GROUP VARCHAR(200) NULL,
    IS_NONCONCURRENT VARCHAR(1) NULL,
    REQUESTS_RECOVERY VARCHAR(1) NULL,
    PRIMARY KEY (SCHED_NAME,ENTRY_ID)
);

CREATE TABLE QRTZ_SCHEDULER_STATE
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    INSTANCE_NAME VARCHAR(200) NOT NULL,
    LAST_CHECKIN_TIME BIGINT(13) NOT NULL,
    CHECKIN_INTERVAL BIGINT(13) NOT NULL,
    PRIMARY KEY (SCHED_NAME,INSTANCE_NAME)
);

CREATE TABLE QRTZ_LOCKS
  (
    SCHED_NAME VARCHAR(120) NOT NULL,
    LOCK_NAME  VARCHAR(40) NOT NULL,
    PRIMARY KEY (SCHED_NAME,LOCK_NAME)
);



