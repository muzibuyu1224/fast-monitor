package com.fast.cloud.monitor.util;

import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/9/12
 * @Version: V1.0
 */
public class JvmFormatUtils {

    /**
     * 分割字符串
     *
     * @param src
     * @param split
     * @return
     */
    public static List<String> split(String src, String split) {
        if (StrUtil.isEmpty(src)) {
            return Lists.newArrayList();
        }
        return Arrays
                .stream(src.split(split))
                .filter(StrUtil::isNotEmpty)
                .collect(Collectors.toList());
    }


    /**
     * 获取值比如  key=value ,输入的key:key=
     *
     * @param srcList
     * @param key
     * @return
     */
    public static String getKeyValue(List<String> srcList, String key) {
        for (String v : srcList) {
            if (v.contains(key)) {
                return v.substring(v.indexOf(key)+key.length());
            }
        }
        return "";
    }


    /**
     * 匹配字符出现次数
     *
     * @param srcText
     * @param findText
     * @return
     */
    public static int appearNumber(String srcText, String findText) {
        int count = 0;
        Pattern p = Pattern.compile(findText);
        Matcher m = p.matcher(srcText);
        while (m.find()) {
            count++;
        }
        return count;
    }


    /**
     * 转成16进制
     *
     * @param val
     * @return
     */
    public static String toHex(String val) {
        int n = Integer.parseInt(val);
        StringBuilder sb = new StringBuilder(8);
        char[] b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        while (n != 0) {
            sb.append(b[n % 16]);
            n = n / 16;
        }
        return sb.reverse().toString().toLowerCase();
    }

    /**
     * 现在时间
     *
     * @return
     */
    public static String time() {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd HH:mm:ss");
        return format.format(new Date());
    }
}
