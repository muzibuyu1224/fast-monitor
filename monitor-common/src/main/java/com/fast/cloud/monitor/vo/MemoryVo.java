package com.fast.cloud.monitor.vo;

import com.fast.cloud.monitor.util.BigDecimalUtil;
import lombok.Data;

/**
 * 內存相关信息
 *
 * @author muzibuyu1224
 */
@Data
public class MemoryVo {
    /**
     * 内存总量
     */
    private double total;

    /**
     * 已用内存
     */
    private double used;

    /**
     * 剩余内存
     */
    private double free;

    private double usage;

    public double getTotal() {
        return BigDecimalUtil.div(total, (1024 * 1024 * 1024), 2);
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public double getUsed() {
        return BigDecimalUtil.div(used, (1024 * 1024 * 1024), 2);
    }

    public void setUsed(long used) {
        this.used = used;
    }

    public double getFree() {
        return BigDecimalUtil.div(free, (1024 * 1024 * 1024), 2);
    }

    public void setFree(long free) {
        this.free = free;
    }

    public double getUsage() {
        return BigDecimalUtil.mul(BigDecimalUtil.div(used, total, 4), 100);
    }
}
