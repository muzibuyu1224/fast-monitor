package com.fast.cloud.monitor.constant;

/**
 * @Description: 前缀路径在配置中完成，这里写的路径具体到mapping
 * @Author: muzibuyu1224
 * @Date: 2021/9/19
 * @Version: V1.0
 */
public interface MonitorUrl {


    String HEAP_FILE = "/fast-actuator/file/heap";

    String THREAD_FILE = "/fast-actuator/file/thread";

    String TINFO_THREAD = "/fast-actuator/info/thread";

    String TINFO_CLASS_LOAD = "/fast-actuator/info/classload";

    String JAVA_HIGH_CPU_STACK = "/fast-actuator/info/javaHighCpuStack";

    String TINFO_GC = "/fast-actuator/info/";//gc
    String TINFO_GC_CAPACITY = "/fast-actuator/info/";//gcCapacity
    String TINFO_GC_META_CAPACITY = "/fast-actuator/info/";//gcMetaCapacity
    String TINFO_GC_NEW = "/fast-actuator/info/";//gcNew
    String TINFO_GC_OLD = "/fast-actuator/info/";//gcOld
    String TINFO_GC_NEW_CAPACITY = "/fast-actuator/info/";//gcNewCapacity
    String TINFO_GC_OLD_CAPACITY = "/fast-actuator/info/";//gcOldCapacity
    String TINFO_GC_UTIL = "/fast-actuator/info/";//gcutil

    String TINFO_JSTAT_CLASS = "/fast-actuator/info/jstatClass";
    String TINFO_MAP_OBJ = "/fast-actuator/info/map-obj";
    String TINFO_MAP_HEAP = "/fast-actuator/info/map-heap";
    String TINFO_EXEC = "/fast-actuator/info/exec";
    String TINFO_LOG_READER = "/fast-actuator/info/logReader";
    String TINFO_SYSTEM_INFO = "/fast-actuator/info/systemInfo";


}
