package com.fast.cloud.monitor;

import com.alibaba.cloud.nacos.registry.NacosRegistration;
import com.fast.cloud.monitor.core.Cmd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description: 存放程序pid
 * @Author: muzibuyu1224
 * @Date: 2021/10/10
 * @Version: V1.0
 */
@Component
public class ApplicationPidSetting {
    @Autowired
    public ApplicationPidSetting(NacosRegistration registration) {
        registration.getMetadata().put("pid", Cmd.getPid(null));
    }
}
