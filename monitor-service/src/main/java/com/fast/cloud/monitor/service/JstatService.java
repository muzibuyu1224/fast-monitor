package com.fast.cloud.monitor.service;

import com.fast.cloud.monitor.core.Jstat;
import com.fast.cloud.monitor.entity.KvModel;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: jstat
 * @Author: muzibuyu1224
 * @Date: 2021/9/12
 * @Version: V1.0
 */
@Service("jstatService")
public class JstatService {


    /**
     * gc 堆内存信息
     * X轴为时间，Y轴为值的变化
     *
     * @param pid
     * @return
     */
    public List<KvModel> gc(String pid) {
        return Jstat.jstatTemplate(pid, Lists.newArrayList(Jstat.GC));
    }


    /**
     * 类加载信息
     * X轴为时间，Y轴为值的变化
     *
     * @param pid
     * @return
     */
    public List<KvModel> jstatClass(String pid) {
        List<KvModel> jstatClass = Jstat.jstatTemplate(pid, Lists.newArrayList("-class"));
        List<KvModel> jstatCompiler = Jstat.jstatTemplate(pid, Lists.newArrayList("-compiler"));
        jstatClass.addAll(jstatCompiler);
        return jstatClass;
    }

    /**
     * 堆内存统计
     *
     * @param pid
     * @return
     */
    public List<KvModel> gcCapacity(String pid) {
        return Jstat.jstatTemplate(pid, Lists.newArrayList(Jstat.GC_CAPACITY));
    }

    /**
     * 新生代垃圾回收统计
     *
     * @param pid
     * @return
     */
    public List<KvModel> gcNew(String pid) {
        return Jstat.jstatTemplate(pid, Lists.newArrayList(Jstat.GC_NEW));
    }

    /**
     * 新生代内存统计
     *
     * @param pid
     * @return
     */
    public List<KvModel> gcNewCapacity(String pid) {
        return Jstat.jstatTemplate(pid, Lists.newArrayList(Jstat.GC_NEW_CAPACITY));
    }

    /**
     * 老年代垃圾回收统计
     *
     * @param pid
     * @return
     */
    public List<KvModel> gcOld(String pid) {
        return Jstat.jstatTemplate(pid, Lists.newArrayList(Jstat.GC_OLD));
    }

    /**
     * 老年代内存统计
     *
     * @param pid
     * @return
     */
    public List<KvModel> gcOldCapacity(String pid) {
        return Jstat.jstatTemplate(pid, Lists.newArrayList(Jstat.GC_OLD_CAPACITY));
    }

    /**
     * 元数据空间统计
     *
     * @param pid
     * @return
     */
    public List<KvModel> gcMetaCapacity(String pid) {
        return Jstat.jstatTemplate(pid, Lists.newArrayList(Jstat.GC_META_CAPACITY));
    }

    /**
     * 堆内存百分比
     * 实时监控
     *
     * @param pid
     * @return
     */
    public List<KvModel> gcutil(String pid) {
        return Jstat.jstatTemplate(pid, Lists.newArrayList(Jstat.GC_UTIL));
    }


    public static void main(String[] args) {
        JstatService jstatService = new JstatService();
        //System.out.println(jstatService.gc("17897"));
        System.out.println(jstatService.gcCapacity("17897"));
        System.out.println();
        System.out.println(jstatService.gcMetaCapacity("17897"));
        System.out.println();

        System.out.println(jstatService.gcNew("17897"));
        System.out.println();

        System.out.println(jstatService.gcOld("17897"));
        System.out.println();

        System.out.println(jstatService.gcNewCapacity("17897"));
        System.out.println();

        System.out.println(jstatService.gcOldCapacity("17897"));
        System.out.println();

        System.out.println(jstatService.gcutil("17897"));
        System.out.println();

        System.out.println(jstatService.jstatClass("17897"));
    }
}
