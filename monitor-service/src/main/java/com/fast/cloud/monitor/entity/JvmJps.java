package com.fast.cloud.monitor.entity;

import lombok.Data;

import java.util.List;

@Data
public class JvmJps {
    //全名
    private String className;
    //小名
    private String smallName;
    //参数
    private List<String> parameters;
}
