package com.fast.cloud.monitor.service;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import com.fast.cloud.monitor.core.Jps;
import com.fast.cloud.monitor.core.Jstack;
import com.fast.cloud.monitor.core.model.TopObj;
import com.fast.cloud.monitor.core.top.Top;
import com.fast.cloud.monitor.entity.JvmJstack;
import com.fast.cloud.monitor.entity.JvmJstackTopCpu;
import com.fast.cloud.monitor.util.JvmFormatUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: jStack服务
 * @Author: muzibuyu1224
 * @Date: 2021/9/12
 * @Version: V1.0
 */
@Service("jStackService")
public class JStackService {

    /**
     * 没有 -H 打印12
     * top -b -d 1 -n 1 -i -c |awk '{ if (NR > 6) print }' |awk '{ if ($9 > 70) print $1, $2, $9, $10,$12}' | sort -rn -k +3 |head -10
     * 获取高cpu的栈信息
     *
     * @param pid      程序pid
     * @param topRow   top命令获取前几条数据
     * @param stackRow stackRow打印多少行栈信息
     * @return
     */
    public List<JvmJstackTopCpu> queryHighCpuStackByPid(String pid, int topRow, int stackRow) {
        // 获取tid
        return null;
    }

    /**
     * 获取cpu占用最高的线程栈代码
     * 目前只支持 linux
     * 有-H 打印13
     * top -H -b -d 1 -n 1 -i -c |awk '{ if (NR > 6) print }' |awk '{ if ($12 == "java") print }' |awk '{ if ($9 > 70) print $1, $2, $9, $10,$13}' | sort -rn -k +3 |head -3
     * top -H -p 3414055 -b -d 1 -n 1 -i -c |awk '{ if (NR > 6) print }' |awk '{ if ($12 == "java") print }' |awk '{ if ($9 > 70) print $1, $2, $9, $10,$13}' | sort -rn -k +3 |head
     * jstack pid|grep -A 10 4cd0
     * 只获取java的内存占用
     *
     * @param pid         pid
     * @param stackRow    filterAppCommand java
     * @param sources     分数，cpu百分比或者mem内存
     * @param topRow      topRow topRow限制一定数量
     * @param stackRow    打印栈行
     * @param filterByMem 是否根据内存排序
     * @return
     */
    public List<JvmJstackTopCpu> queryJavaHighCpuStack(
            String pid,
            String filterAppCommand,
            int sources,
            int topRow,
            Integer stackRow,
            Boolean filterByMem) {
        if (StrUtil.isEmpty(filterAppCommand)) {
            filterAppCommand = "java";
        }

        // 1. 构建top命令
        List<String> cmdList
                = buildTopCmd(pid, filterAppCommand, sources, topRow, filterByMem);

        // 2. 获取top 结果
        String topResult = Top.top(cmdList);

        //tid     user  sources  mem  app
        //3414056 root 99.9 0.8 Math
        if (StrUtil.isEmpty(topResult)) {
            return Lists.newArrayList();
        }
        // 每一行的结果
        List<TopObj> resultList = buildTopResult(topResult);

        // 3. 准备构建就stack命令
        return buildJStackResult(pid, resultList, stackRow);
    }

    /**
     * 准备构建就stack命令
     *
     * @param pid
     * @param resultList
     * @param stackRow
     * @return
     */
    public List<JvmJstackTopCpu> buildJStackResult(String pid, List<TopObj> resultList, Integer stackRow) {
        // 如果知道pid，不需要执行jps
        // 根据app分好组
        Map<String, List<TopObj>> appMap =
                resultList
                        .stream()
                        .collect(Collectors.groupingBy(TopObj::getApp));
        // name pid
        Map<String, String> pidMap = Maps.newHashMap();
        if (StrUtil.isEmpty(pid)) {
            // 根据appName，使用jps获取pid:  jps |awk {'if($2=="Math") print $1}',这里直接一次性获取所有获取所有jps
            Map<String, String> jpsMap = Jps.jpsList();
            for (String k : appMap.keySet()) {
                String jpsPid = jpsMap.get(k);
                if (StrUtil.isNotEmpty(jpsPid)) {
                    pidMap.put(k, jpsPid);
                }
            }
        } else {
            // 传了pid的参数
            String name = Lists.newArrayList(appMap.keySet()).get(0);
            pidMap.put(name, pid);
        }

        if (null == stackRow || stackRow < 4) {
            stackRow = 5;
        }

        // 线程输出的信息
        List<JvmJstackTopCpu> threadInfoList = Lists.newArrayList();

        // 根据 pid  和 tid 获取输出
        // jstack pid|grep -A 10 4cd0
        for (TopObj top : resultList) {
            String temPid = pidMap.get(top.getApp());
            String nid16 = JvmFormatUtils.toHex(top.getPid());
            String threadInfo = Jstack.jStackPrintStr(temPid, null, Lists.newArrayList("|grep -A", stackRow.toString(), nid16));
            if (StrUtil.isEmpty(temPid)) {
                continue;
            }
            JvmJstackTopCpu jstackTopCpu = new JvmJstackTopCpu();
            jstackTopCpu.setThreadContext(threadInfo);
            jstackTopCpu.setPid(temPid);
            jstackTopCpu.setThreadPid(top.getPid());
            jstackTopCpu.setThreadName(top.getApp());
            jstackTopCpu.setCreateTime(LocalDateTime.now());
            threadInfoList.add(jstackTopCpu);
        }
        // 格式输出
        for (JvmJstackTopCpu info : threadInfoList) {
            Jstack.JvmJStackTopCpuBuilder(JvmFormatUtils.split(info.getThreadContext(), "\n"), info);
        }
        return threadInfoList;
    }


    /**
     * top 命令构建
     *
     * @param pid
     * @param appCommand
     * @param sources
     * @param topRow
     * @param filterByMem
     * @return
     */
    protected List<String> buildTopCmd(String pid, String appCommand, int sources, int topRow, Boolean filterByMem) {
        List<String> cmd = Lists.newArrayList();
        cmd.add("-H");
        if (StrUtil.isEmpty(pid)) {
            cmd.add("-p");
            cmd.add(pid);
        }
        cmd.add("-b");
        cmd.add("-d");
        cmd.add("1");
        cmd.add("-n");
        cmd.add("1");
        cmd.add("-i");
        cmd.add("-c");
        cmd.add("|awk '{ if (NR > 6) print }'");
        cmd.add(String.format("|awk '{ if ($12 == \"%s\") print }'", appCommand));
        if (BooleanUtil.isTrue(filterByMem)) {
            // 根据第十列(mem)
            cmd.add(String.format("|awk '{ if ($10 > %s) print $1, $2, $9, $10,$13}'", sources));
            cmd.add(String.format("|sort -rn -k +4 |head %s", -topRow));
        } else {
            // 根据第九列(cpu)
            cmd.add(String.format("|awk '{ if ($9 > %s) print $1, $2, $9, $10,$13}'", sources));
            cmd.add(String.format("|sort -rn -k +3 |head %s", -topRow));
        }
        return cmd;
    }

    /**
     * top结果构建
     *
     * @param topResult
     * @return
     */
    protected List<TopObj> buildTopResult(String topResult) {
        // 获取每一行的输出结果
        String[] topResultArray = topResult.split("\n");
        return Arrays.stream(topResultArray)
                .filter(StrUtil::isNotEmpty)
                .map(x -> {
                    String[] s = x.split(" ");
                    TopObj obj = new TopObj();
                    obj.setPid(s[0]);
                    obj.setUser(s[1]);
                    obj.setCpu(s[2]);
                    obj.setMem(s[3]);
                    obj.setApp(s[4]);
                    return obj;
                })
                .collect(Collectors.toList());
    }


    /**
     * 该进程的线程信息
     * X轴为时间，Y轴为值的变化
     *
     * @param pid
     * @return
     */
    public JvmJstack jStackCount(String pid) {
        return Jstack.jStackCount(pid);
    }


    /**
     * 打印blocked信息
     *
     * @param pid
     * @return
     */
    public String jStackBlocked(String pid) {
        return Jstack.jStackBlocked(pid);
    }

    /**
     * 导出线程快照
     *
     * @return
     */
    public String jStackToFile(String pid) {
        return Jstack.jStackToFile(pid);
    }

    public static void main(String[] args) {
        JStackService jStackService = new JStackService();
        String pid = "";
        TopObj obj1 = new TopObj();
        obj1.setPid("19459");
        obj1.setApp("test.java.Math");
        TopObj obj2 = new TopObj();
        obj2.setPid("10499");
        obj2.setApp("test.java.Math");
        List<TopObj> resultList = Lists.newArrayList(obj1, obj2);
        Integer stackRow = 5;
        List<JvmJstackTopCpu> jvmJstackTopCpus = jStackService.buildJStackResult(pid, resultList, stackRow);
        jStackService.queryJavaHighCpuStack("3970", "java", 50, 1, 4, false);
    }
}
