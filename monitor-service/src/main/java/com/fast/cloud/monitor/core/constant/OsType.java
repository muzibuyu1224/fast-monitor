package com.fast.cloud.monitor.core.constant;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/9/10
 * @Version: V1.0
 */
public interface OsType {

    String LINUX = "Linux";

    String WINDOW = "Window";

    String MACOS = "Mac";
}
