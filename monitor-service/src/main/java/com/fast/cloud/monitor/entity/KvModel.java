package com.fast.cloud.monitor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KvModel {
    private String key;
    private String value;


    public static Map<String,Object> toMap(List<KvModel> list){
        Map<String, Object> map = new HashMap<>(list.size());
        for (KvModel m : list) {
            map.put(m.getKey(), m.getValue());
        }
        return map;
    }
}
