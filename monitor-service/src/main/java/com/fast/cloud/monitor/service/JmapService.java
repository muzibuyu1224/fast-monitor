package com.fast.cloud.monitor.service;

import com.fast.cloud.monitor.core.Jmap;
import org.springframework.stereotype.Service;

/**
 * @Description: JmapService
 * @Author: muzibuyu1224
 * @Date: 2021/9/13
 * @Version: V1.0
 */
@Service("jmapService")
public class JmapService {


    /**
     * 导出堆快照
     *
     * @return
     */
    public String dump(String pid) {
        return Jmap.dump(pid);
    }


    /**
     * 查询对象内存信息
     * jmap -histo:live 9652 | head -n 100
     *
     * @param pid
     * @return
     */
    public String jMapObj(String pid, String row) {
        return Jmap.jMapObj(pid, row);
    }

    /**
     * 查询堆信息
     * jmap -heap 12733
     *
     * @param pid
     * @return
     */
    public String jMapHeap(String pid) {
        return Jmap.jMapHeap(pid);
    }

}
