package com.fast.cloud.monitor.core;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.fast.cloud.monitor.core.exec.CmdExecutor;
import com.fast.cloud.monitor.entity.JvmJstack;
import com.fast.cloud.monitor.entity.JvmJstackTopCpu;
import com.fast.cloud.monitor.util.JvmFormatUtils;
import com.fast.cloud.monitor.utils.PathUtil;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 可查找死锁，可以用jvisualvm自动检测死锁(centos作为标准)
 * 1.  jps 获取pid
 * 2.  top -H pid 获取内存最高的线程的id
 * 3.  线程tid转为16进制，比如19664,转为十六进制得到 0x4cd0，此为线程id的十六进制表示
 * 4. 执行 jstack pid|grep -A 10 4cd0，得到线程堆栈信息中4cd0这个线程所在行的后面10行，
 * 从堆栈中可以发现导致cpu飙高的调用方法
 * <p>
 * <p>
 * <p>
 * | 管道符不会被解析
 * linux下：
 * String[] commands = { "/bin/sh", "-c", command };
 * Process ps = Runtime.getRuntime().exec(commands);
 * windows下：
 * String[] commands = { "cmd", "/c", command};
 *
 * @author muzibuyu1224
 */
@Slf4j
public class Jstack {
    private final static String PREFIX = "java.lang.Thread.State: ";
    private final static String J_STACK = "jstack";

    /**
     * 打印内存消耗高的线程栈信息
     * jstack pid|grep -A 10 4cd0
     *
     * @return
     */
    public static JvmJstackTopCpu jStackPrint(String pid, List<String> inputBaseParam, List<String> inputChannelParam) {
        String threadInfo = jStackPrintStr(pid, inputBaseParam, inputChannelParam);
        JvmJstackTopCpu jstackTopCpu = new JvmJstackTopCpu();
        jstackTopCpu.setPid(pid);
        if (StrUtil.isEmpty(threadInfo)) {
            return jstackTopCpu;
        }
        // 获取第一行
        List<String> outList = JvmFormatUtils.split(threadInfo, "\n");
        JvmJStackTopCpuBuilder(outList, jstackTopCpu);
        jstackTopCpu.setThreadContext(threadInfo);
        return jstackTopCpu;
    }

    /**
     * 执行命令返回结果
     *
     * @param pid
     * @param inputBaseParam
     * @param inputChannelParam
     * @return
     */
    public static String jStackPrintStr(String pid, List<String> inputBaseParam, List<String> inputChannelParam) {
        pid = Cmd.getPid(pid);
        if (CollUtil.isEmpty(inputBaseParam)) {
            inputBaseParam = Lists.newArrayList();
        }
        inputBaseParam.add(pid);
        if (CollUtil.isNotEmpty(inputChannelParam)) {
            inputBaseParam.addAll(inputChannelParam);
        }
        List<String> cmdFormat = Cmd.cmdFormat(J_STACK, inputBaseParam);
        return CmdExecutor.execute(cmdFormat, null, null);
    }


    /**
     * 构建返回结果
     *
     * @param outList
     * @param jstackTopCpu
     * @return
     */
    public static JvmJstackTopCpu JvmJStackTopCpuBuilder(List<String> outList, JvmJstackTopCpu jstackTopCpu) {
        if (null == jstackTopCpu) {
            jstackTopCpu = new JvmJstackTopCpu();
        }
        if (CollUtil.isEmpty(outList)) {
            return jstackTopCpu;
        }
        // main
        String firstStr = outList.get(0);

        // " (?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)" 正则表达式忽略引号中的空格分割
        List<String> split = JvmFormatUtils.split(firstStr, " (?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
        String threadName = split.get(0);
        String nid = JvmFormatUtils.getKeyValue(split, "nid=");
        jstackTopCpu.setThreadName(threadName.replace("\"", ""));
        jstackTopCpu.setThreadNid(nid);
        return jstackTopCpu;
    }

    /**
     * 该进程的线程信息
     * X轴为时间，Y轴为值的变化
     *
     * @return
     */
    public static JvmJstack jStackCount(String pid) {
        pid = Cmd.getPid(pid);
        List<String> cmdFormat = Cmd.cmdFormat(J_STACK, Lists.newArrayList("-l", pid));
        String threadInfo = CmdExecutor.execute(cmdFormat, null, null);
        int total = JvmFormatUtils.appearNumber(threadInfo, "nid=");
        int locked = JvmFormatUtils.appearNumber(threadInfo, "locked <");
        int runnable = JvmFormatUtils.appearNumber(threadInfo, PREFIX + "RUNNABLE");
        int blocked = JvmFormatUtils.appearNumber(threadInfo, PREFIX + "BLOCKED");
        int timedWaiting = JvmFormatUtils.appearNumber(threadInfo, PREFIX + "TIMED_WAITING");
        int waiting = JvmFormatUtils.appearNumber(threadInfo, PREFIX + "WAITING");
        return new JvmJstack(pid, total, runnable, timedWaiting, waiting, locked, blocked,null,null);
    }

    /**
     * 打印blocked信息
     *
     * @param pid
     * @return
     */
    public static String jStackBlocked(String pid) {
        pid = Cmd.getPid(pid);
        List<String> cmdFormat = Cmd.cmdFormat(J_STACK, Lists.newArrayList("-l", pid, "grep -A 15 \"java.lang.Thread.State: BLOCKED\""));
        return CmdExecutor.execute(cmdFormat, null, null);
    }

    /**
     * 导出线程快照
     *
     * @return
     */
    public static String jStackToFile(String pid) {
        pid = Cmd.getPid(pid);
        String path = PathUtil.getRootPath("dump/" + pid + "_thread.txt");
        List<String> cmdFormat = Cmd.cmdFormat(J_STACK, Lists.newArrayList(pid));
        String s = CmdExecutor.execute(cmdFormat, null, null);
        File file = new File(path);
        try {
            FileUtils.write(file, s, StandardCharsets.UTF_8);
            return path;
        } catch (IOException e) {
            log.debug("jStackToFile 导出栈信息到文件失败:{}", e.getMessage());
        }
        return null;
    }
}


// ps aux | grep Math
// linux ps -eLf | grep my-process-name

//
//-F不要计算共享库的统计信息，也称为框架。
//-R不遍历并报告每个进程的内存对象映射。
//-o cpu按CPU使用情况排序

// ps -elo pid,ppid,lwp,nlwp,osz,rss,ruser,pcpu,stime,etime,args

// mac ps -aux |grep


// powershell
//Get-Process -ID pid | Select-Object *
//Get-Process -ID 2699 | Select-Object -ExpandProperty  Threads
//7ba6cf20