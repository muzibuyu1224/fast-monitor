package com.fast.cloud.monitor.core.top;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 还要判断是不是在jps中 如果是用作jstack的话
 * @Author: muzibuyu1224
 * @Date: 2021/9/12
 * @Version: V1.0
 */
@Service("linuxTop")
public class LinuxTop extends BaseTop {
//d : 改变显示的更新速度，或是在交谈式指令列( interactive command)按 s
//q : 没有任何延迟的显示速度，如果使用者是有 superuser 的权限，则 top 将会以最高的优先序执行
//c : 切换显示模式，共有两种模式，一是只显示执行档的名称，另一种是显示完整的路径与名称
//S : 累积模式，会将己完成或消失的子进程 ( dead child process ) 的 CPU time 累积起来
//s : 安全模式，将交谈式指令取消, 避免潜在的危机
//i : 不显示任何闲置 (idle) 或无用 (zombie) 的进程
//n : 更新的次数，完成后将会退出 top
//b : 批次档模式，搭配 "n" 参数一起使用，可以用来将 top 的结果输出到档案内
//获取CPU大于70的进程（根据CPU占用大小，由高到低排序）：
//top -b -d 1 -n 1 |awk '{ if (NR > 6) print }' |awk '{ if ($9 > 70) print $1, $2, $9, $10}' | sort -rn -k +3
//获取内存大于70的进程（根据内存占用大小，由高到低排序）：
//top -b -d 1 -n 1 |awk '{ if (NR > 6) print }' |awk '{ if ($10 > 70) print $1, $2, $9, $10}' | sort -rn -k +4

    @Override
    public List<String> queryTid(String pid, int row) {
        return null;
    }

    @Override
    public List<String> queryPid(int row) {
        return null;
    }
}
