package com.fast.cloud.monitor.core;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.lang.management.ManagementFactory;
import java.util.List;

@Slf4j
@UtilityClass
public class Cmd {
    public static final String OS = ManagementFactory.getOperatingSystemMXBean().getName();
    protected static final List<String> SH = Lists.newArrayList("sh", "-c");
    protected static final List<String> CMD = Lists.newArrayList("cmd", "/c");
    public static final String WINDOW = "Window";
    public static final String CHANNEL = "|";
    public static final String MAC_OS = "Mac";
    public static final String LINUX = "Linux";


    /**
     * 获取当前应用进程id
     *
     * @return
     */
    public static String getPid(String pid) {
        if (StrUtil.isNotEmpty(pid)) {
            return pid;
        }
        String name = ManagementFactory.getRuntimeMXBean().getName();
        return name.split("@")[0];
    }

    /**
     * 命令格式化
     *
     * @param bCmd  基础命令
     * @param param 命令的参数
     * @return
     */
    public List<String> cmdFormat(String bCmd, List<String> param) {
        if (StrUtil.isEmpty(bCmd)) {
            return null;
        }
        // 进制的命令
        if (cmdCheck(bCmd)) {
            return null;
        }
        List<String> cmd = Lists.newArrayList();
        boolean isChannel = contains(param, CHANNEL);
        if (isChannel) {
            if (OS.startsWith(WINDOW)) {
                cmd.addAll(CMD);
            } else {
                cmd.addAll(SH);
            }
        }
        // 如果油管道命令 list逗号分隔，没有的话add
        if (CollUtil.isNotEmpty(param)) {
            if (isChannel) {
                // 油管道命令 所有命令空格分隔
                cmd.add(bCmd + " " + String.join(" ", param));
            } else {
                // 没有管道命令
                cmd.add(bCmd);
                cmd.addAll(param);
            }
        } else {
            cmd.add(bCmd);
        }
        boolean b = cmd.stream().allMatch(Cmd::cmdCheck);
        if (b) {
            return null;
        }
        log.info("run cmd:{}", String.join(" ", cmd));
        return cmd;
    }


    /**
     * 命令校验
     *
     * @param cmd
     * @return
     */
    private boolean cmdCheck(String cmd) {
        String temCmd = cmd.toLowerCase();
        return temCmd.contains("rm")
                || temCmd.contains("sudo")
                || temCmd.contains("su")
                || temCmd.contains("chmod")
                || temCmd.contains("chown");
    }


    /**
     * 判断是否包含某个字段
     * @param list
     * @param key
     * @return
     */
    public boolean contains(List<String> list, String key) {
        if (CollUtil.isEmpty(list)) {
            return false;
        }
        for (String v : list) {
            if (null != v && v.contains(key)) {
                return true;
            }
        }
        return false;
    }
}
