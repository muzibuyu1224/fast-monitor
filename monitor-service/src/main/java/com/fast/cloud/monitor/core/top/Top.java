package com.fast.cloud.monitor.core.top;

import com.fast.cloud.monitor.core.Cmd;
import com.fast.cloud.monitor.core.exec.CmdExecutor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 可查找死锁，可以用jvisualvm自动检测死锁(centos作为标准)
 *
 * @author muzibuyu1224
 */
@Slf4j
public class Top {
    private final static String TOP = "top";

    public static String top(List<String> inputBaseParam) {
        List<String> cmdFormat = Cmd.cmdFormat(TOP, inputBaseParam);
        return CmdExecutor.execute(cmdFormat, null, null);
    }
}