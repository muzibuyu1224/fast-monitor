package com.fast.cloud.monitor.controller;

import com.fast.cloud.framework.core.model.FastResult;
import com.fast.cloud.monitor.core.Cmd;
import com.fast.cloud.monitor.entity.*;
import com.fast.cloud.monitor.service.CmdService;
import com.fast.cloud.monitor.service.JStackService;
import com.fast.cloud.monitor.service.JmapService;
import com.fast.cloud.monitor.service.JstatService;
import com.fast.cloud.monitor.utils.LogReaderUtils;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/9/7
 * @Version: V1.0
 */
@RestController
@RequestMapping("/fast-actuator/info")
public class JvmController {

    @Value("${actuator.log.path}")
    private String logUrl;

    @Resource(name = "jmapService")
    private JmapService jmapService;

    @Resource(name = "jStackService")
    private JStackService jStackService;

    @Resource(name = "jstatService")
    private JstatService jstatService;

    @Resource(name = "cmdService")
    private CmdService cmdService;


    @GetMapping("/javaHighCpuStack")
    public FastResult<List<JvmJstackTopCpu>> queryJavaHighCpuStack(@RequestParam("pid") String pid,
                                                                   int sources,
                                                                   int topRow,
                                                                   Integer stackRow,
                                                                   Boolean filterByMem) {
        try {
            List<JvmJstackTopCpu> result = jStackService.queryJavaHighCpuStack(pid, "java", sources, topRow, stackRow, filterByMem);
            return FastResult.build(result);
        } catch (Exception e) {
            return FastResult.build(Lists.newArrayList());
        }
    }

    /**
     * 线程信息
     *
     * @return
     */

    @GetMapping("/thread")
    public FastResult<JvmJstack> thread(@RequestParam("pid") String pid) {
        try {
            JvmJstack jvmJstack = jStackService.jStackCount(pid);
            jvmJstack.setCreateTime(LocalDateTime.now());
            jStackService.jStackCount(pid);
            return FastResult.build(jvmJstack);
        } catch (Exception e) {
            e.printStackTrace();
            return FastResult.build();
        }
    }


    /**
     * 类加载信息
     *
     * @return
     */
    @GetMapping("/classload")
    public FastResult<JvmClassLoad> classload(@RequestParam("pid") String pid) {
        try {
            List<KvModel> jstatClass = jstatService.jstatClass(pid);
            JvmClassLoad entity = new JvmClassLoad();
            entity.setPid(Integer.valueOf(Cmd.getPid(pid)));
            entity.setCreateTime(LocalDateTime.now());
            entity.setLoaded(jstatClass.get(0).getValue());
            entity.setLoadedBytes(jstatClass.get(1).getValue());
            entity.setUnloaded(jstatClass.get(2).getValue());
            entity.setUnloadedBytes(jstatClass.get(3).getValue());
            entity.setLoadedTime(jstatClass.get(4).getValue());
            entity.setCompiled(jstatClass.get(5).getValue());
            entity.setFailed(jstatClass.get(6).getValue());
            entity.setInvalid(jstatClass.get(7).getValue());
            entity.setUpLoadTime(jstatClass.get(8).getValue());
            return FastResult.build(entity);
        } catch (Exception e) {
            e.printStackTrace();
            return FastResult.build();
        }
    }


    /**
     * gc信息  堆内存信息
     *
     * @return
     */
    @GetMapping("/gc")
    public FastResult<JvmGc> gc(@RequestParam("pid") String pid) {
        try {
            List<KvModel> kvEntities = jstatService.gc(pid);
            JvmGc entity = new JvmGc();
            entity.setId(Integer.valueOf(Cmd.getPid(pid)));
            entity.setCreateTime(LocalDateTime.now());
            entity.setS0C(kvEntities.get(0).getValue());
            entity.setS1C(kvEntities.get(1).getValue());
            entity.setS0U(kvEntities.get(2).getValue());
            entity.setS1U(kvEntities.get(3).getValue());
            entity.setEC(kvEntities.get(4).getValue());
            entity.setEU(kvEntities.get(5).getValue());
            entity.setOC(kvEntities.get(6).getValue());
            entity.setOU(kvEntities.get(7).getValue());
            entity.setMC(kvEntities.get(8).getValue());
            entity.setMU(kvEntities.get(9).getValue());
            entity.setCCSC(kvEntities.get(10).getValue());
            entity.setCCSU(kvEntities.get(11).getValue());
            entity.setYGC(kvEntities.get(12).getValue());
            entity.setYGCT(kvEntities.get(13).getValue());
            entity.setFGC(kvEntities.get(14).getValue());
            entity.setFGCT(kvEntities.get(15).getValue());
            entity.setGCT(kvEntities.get(16).getValue());
            return FastResult.build(entity);
        } catch (Exception e) {
            e.printStackTrace();
            return FastResult.build();
        }
    }


    /**
     * createTimeModel
     *
     * @param result
     */
    private void createTimeModel(Map<String, Object> result) {
        result.putIfAbsent("createTime", LocalDateTime.now());
    }


    /**
     * 堆内存统计
     *
     * @param pid
     * @return
     */
    @GetMapping("/gcCapacity")
    public FastResult<Map<String, Object>> gcCapacity(@RequestParam("pid") String pid) {
        List<KvModel> kvEntities = jstatService.gcCapacity(pid);
        Map<String, Object> result = KvModel.toMap(kvEntities);
        createTimeModel(result);
        return FastResult.build(result);
    }


    /**
     * 元数据空间统计
     *
     * @param pid
     * @return
     */
    @GetMapping("/gcMetaCapacity")
    public FastResult<Map<String, Object>> gcMetaCapacity(@RequestParam("pid") String pid) {
        List<KvModel> kvEntities = jstatService.gcMetaCapacity(pid);
        Map<String, Object> result = KvModel.toMap(kvEntities);
        createTimeModel(result);
        return FastResult.build(result);
    }

    /**
     * 新生代垃圾回收统计
     *
     * @param pid
     * @return
     */
    @GetMapping("/gcNew")
    public FastResult<Map<String, Object>> gcNew(@RequestParam("pid") String pid) {
        List<KvModel> kvEntities = jstatService.gcNew(pid);
        Map<String, Object> result = KvModel.toMap(kvEntities);
        createTimeModel(result);
        return FastResult.build(result);
    }

    /**
     * 老年代垃圾回收统计
     *
     * @param pid
     * @return
     */
    @GetMapping("/gcOld")
    public FastResult<Map<String, Object>> gcOld(@RequestParam("pid") String pid) {
        List<KvModel> kvEntities = jstatService.gcOld(pid);
        Map<String, Object> result = KvModel.toMap(kvEntities);
        createTimeModel(result);
        return FastResult.build(result);
    }


    /**
     * 新生代内存统计
     *
     * @param pid
     * @return
     */
    @GetMapping("/gcNewCapacity")
    public FastResult<Map<String, Object>> gcNewCapacity(@RequestParam("pid") String pid) {
        List<KvModel> kvEntities = jstatService.gcNewCapacity(pid);
        Map<String, Object> result = KvModel.toMap(kvEntities);
        createTimeModel(result);
        return FastResult.build(result);
    }

    /**
     * 老年代内存统计
     *
     * @param pid
     * @return
     */
    @GetMapping("/gcOldCapacity")
    public FastResult<Map<String, Object>> gcOldCapacity(@RequestParam("pid") String pid) {
        List<KvModel> kvEntities = jstatService.gcOldCapacity(pid);
        Map<String, Object> result = KvModel.toMap(kvEntities);
        createTimeModel(result);
        return FastResult.build(result);
    }


    @GetMapping("/gcutil")
    public FastResult<Map<String, Object>> gcutil(@RequestParam("pid") String pid) {
        List<KvModel> kvEntities = jstatService.gcutil(pid);
        Map<String, Object> result = KvModel.toMap(kvEntities);
        createTimeModel(result);
        return FastResult.build(result);
    }

    /**
     * 类加载信息 X轴为时间，Y轴为值的变化
     *
     * @param pid
     * @return
     */

    @GetMapping("/jstatClass")
    public FastResult<Map<String, Object>> jstatClass(@RequestParam("pid") String pid) {
        List<KvModel> kvEntities = jstatService.jstatClass(pid);
        Map<String, Object> result = KvModel.toMap(kvEntities);
        createTimeModel(result);
        return FastResult.build(result);
    }


    /**
     * 查询对象内存信息
     * jmap -histo:live 9652 | head -n 100
     *
     * @param pid
     * @return
     */
    @GetMapping("/map-obj")
    public FastResult<String> jMapObj(@RequestParam("pid") String pid, @RequestParam("row") String row) {
        return FastResult.build(jmapService.jMapObj(pid, row));
    }

    /**
     * 查询堆信息
     * jmap -heap 12733
     *
     * @param pid
     * @return
     */
    @GetMapping("/map-heap")
    public FastResult<String> jMapHeap(@RequestParam("pid") String pid) {
        return FastResult.build(jmapService.jMapHeap(pid));
    }

    /**
     * 执行jvm linux命令
     *
     * @return
     */
    @PostMapping("/exec")
    public FastResult<String> getRuntimeExec(List<String> cmd) {
        try {
            return FastResult.build(cmdService.cmdExec("", cmd));
        } catch (Exception e) {
            return FastResult.build(e.getMessage());
        }
    }

    @RequestMapping("/logReader")
    public FastResult<String> logred(String url) {
        try {
            return FastResult.build(LogReaderUtils.poll(logUrl));
        } catch (Exception e) {
            return FastResult.build(e.getMessage());
        }
    }

    /**
     * 系统物理内存
     *
     * @return
     */

    @GetMapping("/systemInfo")
    public FastResult<ServerVo> systemInfo() {
        try {
            ServerVo server = new ServerVo();
            server.copyTo();
            return FastResult.build(server);
        } catch (Exception e) {
            e.printStackTrace();
            return FastResult.build();
        }
    }
}
