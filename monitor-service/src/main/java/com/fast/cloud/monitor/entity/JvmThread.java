package com.fast.cloud.monitor.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JvmThread {
	/**
	 * 当前应用进程ID
	 */
    private Integer pid;
	/**
	 * 当前应用名称  配置文件 spring.application.name=xxxx
	 */
	private String applicationName;
	/**
	 * 总线程数
	 */
	private int total;
	/**
	 * 运行中的线程
	 */
    private int runnable;
	/**
	 * 这个状态就是有限的(时间限制)的WAITING, 一般出现在调用wait(long), join(long)等情况下, 另外一个线程sleep后
	 * 休眠的线程数
	 */
	private int timedWaiting;
	/**
	 * 这个状态下是指线程拥有了某个锁之后, 调用了他的wait方法, 等待的线程数
	 */
    private int waiting;

	private LocalDateTime createTime;
}
