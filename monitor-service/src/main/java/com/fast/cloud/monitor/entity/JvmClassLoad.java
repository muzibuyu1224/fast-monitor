package com.fast.cloud.monitor.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class JvmClassLoad {
    /**
     * 当前应用进程ID
     */
    private Integer pid;

    /**
     * 当前应用名称  配置文件 spring.application.name=xxxx
     */
    private String applicationName;


    /**
     * 表示载入了类的数量
     */
    private String loaded;

    /**
     * 表示载入了类的合计
     */
    private String loadedBytes;

    /**
     * 表示卸载类的数量
     */
    private String unloaded;

    /**
     * 表示卸载类合计大小
     */
    private String unloadedBytes;
    /**
     * 表示加载和卸载类总共的耗时
     */
    private String loadedTime;
    /**
     * 表示编译任务执行的次数
     */
    private String compiled;
    /**
     * 表示编译失败的次数
     */
    private String failed;
    /**
     * 表示编译不可用的次数
     */
    private String invalid;
    /**
     * 表示编译的总耗时
     */
    private String upLoadTime;

    private LocalDateTime createTime;

}
