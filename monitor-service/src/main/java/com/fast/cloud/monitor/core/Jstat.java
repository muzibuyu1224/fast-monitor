package com.fast.cloud.monitor.core;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.fast.cloud.monitor.entity.KvModel;
import com.fast.cloud.monitor.core.exec.CmdExecutor;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * gc分析
 *
 * @author muzibuyu1224
 */
public class Jstat {
    public final static String J_STAT = "jstat";
    public final static String GC = "-gc";
    public final static String GC_CAPACITY = "-gccapacity";
    public final static String GC_NEW = "-gcnew";
    public final static String GC_NEW_CAPACITY = "-gcnewcapacity";
    public final static String GC_OLD = "-gcold";
    public final static String GC_OLD_CAPACITY = "-gcoldcapacity";
    public final static String GC_META_CAPACITY = "-gcmetacapacity";
    public final static String GC_UTIL = "-gcutil";


    /**
     * Jstat 模板方法
     *
     * @param cmdParam 命令
     * @return 集合
     */
    public static List<KvModel> jstatTemplate(String pid, List<String> cmdParam) {
        pid = Cmd.getPid(pid);
        List<KvModel> list = new ArrayList<>();
        if (CollUtil.isEmpty(cmdParam)) {
            cmdParam = Lists.newArrayList();
        }
        cmdParam.add(pid);
        cmdParam =  Cmd.cmdFormat(J_STAT, cmdParam);
        String result = CmdExecutor.execute(cmdParam, null, null);
        if (StrUtil.isEmpty(result)) {
            return Lists.newArrayList();
        }
        String[] split = result.trim().split("\n");
        String[] keys = split[0].trim().split("\\s+|\t");
        String[] values = split[1].trim().split("\\s+|\t");
        // 特殊情况
        if (cmdParam.get(1).equals("-compiler")) {
            for (int i = 0; i < 4; i++) {
                list.add(new KvModel(keys[i], values[i]));
            }
            return list;
        }
        // 正常流程
        for (int i = 0; i < keys.length; i++) {
            list.add(new KvModel(keys[i], values[i]));
        }
        return list;
    }
}
