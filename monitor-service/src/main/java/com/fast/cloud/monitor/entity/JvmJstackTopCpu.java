package com.fast.cloud.monitor.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * JvmJstack
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JvmJstackTopCpu {
    private String pid;
    private String threadPid;
    private String threadNid;
    private String applicationName;
    private String threadName;
    private String threadContext;
    private LocalDateTime createTime;
}
