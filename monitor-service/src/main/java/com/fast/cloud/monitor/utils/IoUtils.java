package com.fast.cloud.monitor.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/9/8
 * @Version: V1.0
 */
public class IoUtils {

    public static void closeIo(
            InputStreamReader inputStreamReader,
            BufferedReader bufferedReader,
            Process p) {
        if (null != bufferedReader) {
            try {
                bufferedReader.close();
            } catch (IOException e) {
            }
        }
        if (null != inputStreamReader) {
            try {
                inputStreamReader.close();
            } catch (IOException e) {
            }
        }
        if (null != p) {
            p.destroy();
        }
    }

}
