package com.fast.cloud.monitor.core.exec;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.List;

/**
 * 命令执行
 * https://blog.csdn.net/yanxinduan/article/details/81131277?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_title~default-1.no_search_link&spm=1001.2101.3001.4242
 *
 * @author muzibuyu1224
 */
@Slf4j
public class CmdExecutor {

    /**
     * 执行外部程序,并获取标准输出
     * @param cmd
     * @param env
     * @param dir
     * @param encoding
     * @return
     */
    public static String execute(List<String> cmd, List<String> env, File dir, String... encoding) {
        BufferedReader bufferedReader = null;
        InputStreamReader inputStreamReader = null;
        Process p = null;
        try {
            if (env == null) {
                env = Lists.newArrayList();
            }
            p = Runtime.getRuntime().exec(cmd.toArray(new String[0]),env.toArray(new String[0]),dir);

            // 为"错误输出流"单独开一个线程读取之,否则会造成标准输出流的阻塞
            Thread t = new Thread(new InputStreamRunnable(p.getErrorStream(), "ErrorStream"));
            t.start();

            /* "标准输出流"就在当前方法中读取 */
            BufferedInputStream bis = new BufferedInputStream(p.getInputStream());

            if (encoding != null && encoding.length != 0) {
                inputStreamReader = new InputStreamReader(bis, encoding[0]);// 设置编码方式
            } else {
                inputStreamReader = new InputStreamReader(bis, "utf-8");
            }
            bufferedReader = new BufferedReader(inputStreamReader);

            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
            return sb.toString();




//
//
//            // command process
//            ProcessBuilder processBuilder = new ProcessBuilder();
//            processBuilder.c(command);
//            processBuilder.redirectErrorStream(true);
//
//            Process process = processBuilder.start();
//            //Process process = Runtime.getRuntime().exec(command);
//
//            BufferedInputStream bufferedInputStream = new BufferedInputStream(process.getInputStream());
//            bufferedReader = new BufferedReader(new InputStreamReader(bufferedInputStream));
//
//            // command log
//            String line;
//            while ((line = bufferedReader.readLine()) != null) {
//                XxlJobHelper.log(line);
//            }
//
//            // command exit
//            process.waitFor();
//            exitValue = process.exitValue();
        } catch (Exception e) {
            log.debug("命令执行异常:{}", e.getMessage());
            return null;
        } finally {
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != bufferedReader) {
                try {
                    inputStreamReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != p) {
                p.destroy();
            }
        }
    }


    public static void main(String[] args) {
//        execute(new String[]{"/usr/local/microsoft/powershell/7/pwsh ; exit; Get-Process -ID 2699 | Select-Object -ExpandProperty  Threads"});
    }
}
