package com.fast.cloud.monitor.entity;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * JvmJstack
 */
@Data
@AllArgsConstructor
public class JvmJstack {
    private String id;
    private int total;
    private int runnable;
    private int timedWaiting;
    private int waiting;
    private int locked;
    private int blocked;
    private String applicationName;
    private LocalDateTime createTime;
}
