package com.fast.cloud.monitor.utils;

import java.io.File;


/**
 * @author muzibuyu1224
 */
public class JavaHome {
    public final String path = System.getenv("JAVA_HOME") + File.separator + "bin" + File.separator;

}
