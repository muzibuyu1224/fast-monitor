package com.fast.cloud.monitor.core;

import com.fast.cloud.monitor.core.exec.CmdExecutor;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * @Description: 在线查看文件日记内容
 * @Author: muzibuyu1224
 * @Date: 2021/9/12
 * @Version: V1.0
 */
public class Grep {

    public static final String CAT = "cat";
    public static final String GREP = "grep";

    /**
     * @param logFile 要查看的文件 全路径
     * @param keyword 关键字
     * @return
     */
    public static String catFindLog(String logFile, String keyword) {
        List<String> cmdFormat = Cmd.cmdFormat(GREP, Lists.newArrayList("-n", logFile, "|grep", keyword));
        // 获取行号
        return CmdExecutor.execute(cmdFormat, null, null);
    }

    /**
     * grep -A 100 -B 100  'keyword' file
     *
     * @param logFile 要查看的文件 全路径
     * @param keyword 关键字
     * @param row     返回前后row行
     * @return
     */
    public static String grepFindLog(String logFile, String keyword, String row) {
        List<String> cmdFormat = Cmd.cmdFormat(GREP, Lists.newArrayList("-n", "-A", row, "-B", row, keyword, logFile));
        // 获取行号
        return CmdExecutor.execute(cmdFormat, null, null);
    }
}
