package com.fast.cloud.monitor.core.top;

import java.util.List;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/9/12
 * @Version: V1.0
 */
public abstract class BaseTop {

    /**
     * 获取tid
     * @param pid pid
     * @param row row
     * @return
     */
    abstract List<String> queryTid(String pid, int row);


    /**
     * 获取pid
     * @param row
     * @return
     */
    abstract List<String> queryPid(int row);
}
