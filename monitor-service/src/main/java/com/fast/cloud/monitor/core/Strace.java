package com.fast.cloud.monitor.core;

import cn.hutool.core.collection.CollUtil;
import com.fast.cloud.monitor.core.exec.CmdExecutor;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * @Description: 查看系统调用和花费的时间
 * @Author: muzibuyu1224
 * @Date: 2021/9/12
 * @Version: V1.0
 */
public class Strace {
    private final static String STRACE = "strace";

    /**
     * 查看系统调用和花费的时间
     * strace -T -r -c -p pid
     * -c 统计信息
     *
     * @param pid
     * @return
     */
    public static String strace(String pid) {
        pid = Cmd.getPid(pid);
        List<String> cmdFormat = Cmd.cmdFormat(STRACE, Lists.newArrayList("-T", "-r", "-c", "-p", pid));
        return CmdExecutor.execute(cmdFormat, null, null);
    }

    /**
     * strace
     *
     * @param pid
     * @param cmdParam
     * @return
     */
    public static String strace(String pid, List<String> cmdParam) {
        pid = Cmd.getPid(pid);
        if (CollUtil.isEmpty(cmdParam)) {
            cmdParam = Lists.newArrayList();
        }
        cmdParam.add(pid);
        List<String> cmdFormat = Cmd.cmdFormat(STRACE, cmdParam);
        return CmdExecutor.execute(cmdFormat, null, null);
    }
}
