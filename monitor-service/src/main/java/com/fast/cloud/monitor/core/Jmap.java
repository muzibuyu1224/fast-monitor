package com.fast.cloud.monitor.core;


import cn.hutool.core.collection.CollUtil;
import com.fast.cloud.monitor.core.exec.CmdExecutor;
import com.fast.cloud.monitor.utils.PathUtil;
import com.google.common.collect.Lists;

import java.io.File;
import java.util.List;


/**
 * jmap -histo pid
 * jmap -dump:format=b,file=heap.bin pid  导出堆信，可以用jvisualvm命令工具导入该dump文件分析
 * <p>
 * 远程连接jvisualvm 启动普通的jar程序JMX端口配置
 * java‐Dcom.sun.management.jmxremote.port=8888‐Djava.rmi.server.hostname=ip(127.0.0.1)‐Dcom.sun.management.jmxremot e.ssl=false ‐Dcom.sun.management.jmxremote.authenticate=false ‐jar server.jar
 * -Dcom.sun.management.jmxremote.port 为远程机器的JMX端口
 * -Djava.rmi.server.hostname 为远程机器IP
 * <p>
 * tomcat的JMX配置:在catalina.sh文件里的最后一个JAVA_OPTS的赋值语句下一行增加如下配置行
 * JAVA_OPTS="$JAVA_OPTS‐Dcom.sun.management.jmxremote.port=8888‐Djava.rmi.server.hostname=ip‐Dcom.sun.ma nagement.jmxremote.ssl=false ‐Dcom.sun.management.jmxremote.authenticate=false"
 *
 * @author muzibuyu1224
 */
public class Jmap {
    private final static String J_MAP = "jmap";

    /**
     * 导出堆快照
     *
     * @return
     */
    public static String dump(String pid) {
        String id = Cmd.getPid(pid);
        String path = PathUtil.getRootPath("dump/" + id + "_heap.hprof");
        File file = new File(PathUtil.getRootPath("dump/"));
        if (!file.exists()) {
            file.mkdirs();
        }
        List<String> cmdFormat = Cmd.cmdFormat(J_MAP, Lists.newArrayList("-dump:format=b,file="+path, id));
        CmdExecutor.execute(cmdFormat, null, null);
        return path;
    }


    /**
     * 查询对象内存信息
     * jmap -histo:live 9652 | head -n 100
     *
     * @param pid
     * @return
     */
    public static String jMapObj(String pid, String row) {
        pid = Cmd.getPid(pid);
        List<String> cmdFormat = Cmd.cmdFormat(J_MAP, Lists.newArrayList("-histo:live", pid, "|head -n " + row));
        return CmdExecutor.execute(cmdFormat, null, null);
    }

    /**
     * 查询堆信息
     * jmap -heap 12733
     *
     * @param pid
     * @return
     */
    public static String jMapHeap(String pid) {
        return jMap(pid, Lists.newArrayList("-heap"));
    }


    /**
     * jMap
     *
     * @param pid
     * @param inputParam
     * @return
     */
    public static String jMap(String pid, List<String> inputParam) {
        pid = Cmd.getPid(pid);
        if (CollUtil.isEmpty(inputParam)) {
            inputParam = Lists.newArrayList();
        }
        inputParam.add(pid);
        List<String> cmdFormat = Cmd.cmdFormat(J_MAP, inputParam);
        return CmdExecutor.execute(cmdFormat, null, null);
    }
}
