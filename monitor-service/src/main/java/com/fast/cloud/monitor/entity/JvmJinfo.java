package com.fast.cloud.monitor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class JvmJinfo {
    private List<String> noedefault;
    private List<String> commandLine;
}
