package com.fast.cloud.monitor.core;

import cn.hutool.core.collection.CollUtil;
import com.fast.cloud.monitor.core.exec.CmdExecutor;
import com.fast.cloud.monitor.entity.JvmJinfo;
import com.fast.cloud.monitor.util.ArrayUtil;
import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 注意mac系统的区别还有还有jdk的区别
 *
 * 查看jvm的参数
 * jinfo -flags pid
 * <p>
 * 查看java系统参数
 * jinfo -sysprops pid
 *
 * @author muzibuyu1224
 */
public class Jinfo {
    private final static String J_INFO = "jinfo";

    /**
     * JVM默认参数与指定参数
     *
     * @return
     */
    public static JvmJinfo infoFlags(String pid) {
        pid = Cmd.getPid(pid);
        // macos m1 没有-flags 和 -sysprops
        List<String> cmdFormat = Cmd.cmdFormat(J_INFO, Lists.newArrayList("-flags", pid));
        String result = CmdExecutor.execute(cmdFormat, null, null);
        if (!result.contains("successfully")) {
            return null;
        }
        String flags = "flags:";
        String command = "Command line:";
        //默认参数
        String[] noedefault = ArrayUtil.trim(result.substring(result.indexOf(flags) + flags.length(), result.indexOf(command)).split("\\s+"));
        String[] commandLine = null;
        result = result.substring(result.indexOf(command));
        if (!result.equals(command)) {
            commandLine = result.substring(command.length()).split("\\s+");
        }
        commandLine = ArrayUtil.trim(commandLine);
        return new JvmJinfo(
                Arrays.stream(noedefault).collect(Collectors.toList()),
                Arrays.stream(commandLine).collect(Collectors.toList()));
    }


    /**
     * @param pid
     * @param inputParam
     * @return
     */
    public static String info(String pid, List<String> inputParam) {
        pid = Cmd.getPid(pid);
        if (CollUtil.isEmpty(inputParam)) {
            inputParam = Lists.newArrayList();
        }
        inputParam.add(pid);
        List<String> cmdFormat = Cmd.cmdFormat(J_INFO, inputParam);
        return CmdExecutor.execute(cmdFormat, null, null);
    }

}
