package com.fast.cloud.monitor.core;

import cn.hutool.core.util.StrUtil;
import com.fast.cloud.monitor.entity.KvModel;
import com.fast.cloud.monitor.core.exec.CmdExecutor;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Jps {
    public final static String JPS = "jps";

//    /**
//     * Jps 命令结果
//     * key：进程Id
//     *
//     * @return map
//     */
//    public static Map<String, JvmJps> jps() {
//        Map<String, JvmJps> map = new HashMap<>();
//        String s = CmdExecutor.execute(new String[]{"jps", "-l", "-v"});
//        String[] line = s != null ? s.split("\n") : new String[0];
//        for (String aLine : line) {
//            String[] one = aLine.split("\\s+");
//            //排除sun.tools进程
//            if (one[1].contains("sun.tools")) {
//                continue;
//            }
//            //格式化控制台输出
//            if (!one[1].substring(0, 1).equals("-")) {
//                String smallName = one[1].contains(".") ? one[1].substring(one[1].lastIndexOf(".") + 1) : one[1];
//                smallName = smallName.equalsIgnoreCase("jar") ? one[1] : smallName;
//                map.put(one[0], new JpsEntity(one[1], smallName, Arrays.stream(one).skip(2).collect(Collectors.toList())));
//            } else {
//                map.put(one[0], new JpsEntity("NULL", "NULL", Arrays.stream(one).skip(1).collect(Collectors.toList())));
//            }
//            //测试jinfo
//            JvmJinfo info = JvmJinfo.info(one[0]);
//        }
//        return map;
//    }

    /**
     * 获取所有pid集合
     * @return
     */
    public static Map<String,String> jpsList(){
        String jpsValue = CmdExecutor.execute(Lists.newArrayList(JPS,"-l"), null, null);
        if (StrUtil.isEmpty(jpsValue)) {
            return Maps.newHashMap();
        }
        String[] split = jpsValue.split("\n");
        return Arrays.stream(split).filter(x -> StrUtil.isNotEmpty(x) && x.split(" ").length > 1).map(x -> {
            String[] v = x.split(" ");
            //com.test.Math:pid
            return new KvModel(v[1], v[0]);
        }).collect(Collectors.toMap(KvModel::getKey, KvModel::getValue));
    }

    /**
     * 根据名称获取pid集合
     * @param name
     * @return
     */
    public static String jpsPidByName(String name){
        List<String> cmdFormat = Cmd.cmdFormat(JPS, Lists.newArrayList("|awk {'if($2==\""+name+"\") print $1}'"));
        return CmdExecutor.execute(cmdFormat, null, null);
    }

    /**
     * 根据pid获取名称
     * @param pid
     * @return
     */
    public static String jpsNameByPid(String pid){
        List<String> cmdFormat = Cmd.cmdFormat(JPS, Lists.newArrayList("|awk {'if($1==\""+pid+"\") print $2}'"));
        return CmdExecutor.execute(cmdFormat, null, null);
    }
}
