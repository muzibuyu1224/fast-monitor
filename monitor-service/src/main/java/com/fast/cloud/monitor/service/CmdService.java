package com.fast.cloud.monitor.service;

import cn.hutool.core.collection.CollUtil;
import com.fast.cloud.monitor.core.*;
import com.fast.cloud.monitor.entity.JvmJinfo;
import com.fast.cloud.monitor.core.exec.CmdExecutor;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: cmd
 * @Author: muzibuyu1224
 * @Date: 2021/9/13
 * @Version: V1.0
 */
@Service("cmdService")
public class CmdService {


    /**
     * 直接执行命令
     *
     * @param baseCmd
     * @param inputCmd
     * @return
     */
    public String cmdExec(String baseCmd, List<String> inputCmd) {
        if (CollUtil.isEmpty(inputCmd)) {
            inputCmd = Lists.newArrayList();
        }
        List<String> cmdFormat = Cmd.cmdFormat(baseCmd, inputCmd);
        return CmdExecutor.execute(cmdFormat, null, null);
    }

    /**
     * 带pid的执行命令
     *
     * @param baseCmd
     * @param pid
     * @param inputCmd
     * @param channelCmd
     * @return
     */
    public String cmdExec(String baseCmd, String pid, List<String> inputCmd, List<String> channelCmd) {
        pid = Cmd.getPid(pid);
        if (CollUtil.isEmpty(inputCmd)) {
            inputCmd = Lists.newArrayList();
        }
        inputCmd.add(pid);
        if (CollUtil.isNotEmpty(channelCmd)) {
            inputCmd.addAll(channelCmd);
        }
        List<String> cmdFormat = Cmd.cmdFormat(baseCmd, inputCmd);
        return CmdExecutor.execute(cmdFormat, null, null);
    }

    /**
     * grep -A 100 -B 100  'keyword' file
     *
     * @param logFile 要查看的文件 全路径
     * @param keyword 关键字
     * @param row     返回前后row行
     * @return
     */
    public String grepFindLog(String logFile, String keyword, String row) {
        return Grep.grepFindLog(logFile, keyword, row);
    }

    /**
     * 查看系统调用和花费的时间
     * strace -T -r -c -p pid
     * -c 统计信息
     *
     * @param pid
     * @return
     */
    public String strace(String pid) {
        return Strace.strace(pid);
    }

    /**
     * 获取java version
     */
    public String jdkVersion() {
        return CmdExecutor.execute(Lists.newArrayList("java", "-version"), null, null);
    }

    /**
     * JVM默认参数与指定参数
     *
     * @param pid
     * @return
     */
    public JvmJinfo infoFlags(String pid) {
        return Jinfo.infoFlags(pid);
    }


    /**
     * 根据名字获取pid
     * @param name
     * @return
     */
    public String getPidByName(String name){
        return Jps.jpsPidByName(name);
    }


    /**
     * 根据pid获取名字
     * @param pid
     * @return
     */
    public String getNameByPid(String pid){
        return Jps.jpsNameByPid(pid);
    }


}
