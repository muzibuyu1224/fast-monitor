package com.fast.cloud.monitor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * JvmGc
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JvmGc implements Serializable {
    /**
     * 当前应用进程ID
     */
    private Integer id;
    /**
     * 当前应用名称  配置文件 spring.application.name=xxxx
     */
    private String applicationName;

    /**
     * Survivor0空间的大小。单位KB。
     */
    private String S0C;
    /**
     * Survivor1空间的大小。单位KB。
     */
    private String S1C;
    /**
     * Survivor0已用空间的大小。单位KB
     */
    private String S0U;
    /**
     * Survivor1已用空间的大小。单位KB。
     */
    private String S1U;
    /**
     * Eden空间的大小。单位KB。
     */
    private String EC;
    /**
     * Eden已用空间的大小。单位KB。
     */
    private String EU;
    /**
     * 老年代空间的大小。单位KB。
     */
    private String OC;
    /**
     * 老年代已用空间的大小。单位KB。
     */
    private String OU;
    /**
     * 元空间的大小（Metaspace）
     */
    private String MC;
    /**
     * 元空间已使用大小（KB）
     */
    private String MU;
    /**
     * 压缩类空间大小（compressed class space）
     */
    private String CCSC;
    /**
     * 压缩类空间已使用大小（KB）
     */
    private String CCSU;
    /**
     * 新生代gc次数
     */
    private String YGC;
    /**
     * 新生代gc耗时（秒）
     */
    private String YGCT;
    /**
     * Full gc次数
     */
    private String FGC;
    /**
     * Full gc耗时（秒）
     */
    private String FGCT;
    /**
     * gc总耗时（秒）
     */
    private String GCT;

    private LocalDateTime createTime;

}
