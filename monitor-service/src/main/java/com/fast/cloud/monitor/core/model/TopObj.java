package com.fast.cloud.monitor.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 * @Author: muzibuyu1224
 * @Date: 2021/9/12
 * @Version: V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TopObj {
    private String pid;
    private String user;
    private String pr;
    private String ni;
    private String virt;
    private String res;
    private String shr;
    private String s;
    private String cpu;
    private String mem;
    private String time;
    private String command;
    private String app;
}
