//package com.fast.cloud.monitor;
//
//import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
//import com.alibaba.cloud.nacos.ribbon.ConditionalOnRibbonNacos;
//import com.alibaba.nacos.api.NacosFactory;
//import com.alibaba.nacos.api.exception.NacosException;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @Description:
// * @Author: muzibuyu1224
// * @Date: 2021/10/11
// * @Version: V1.0
// */
//@Configuration
//@ConditionalOnRibbonNacos
//public class NacosDynamicServerListConfiguration {
//    @Bean
//    public NacosDynamicServerListUpdater dynamicServerListUpdater(NacosDiscoveryProperties properties) throws NacosException {
//        return new NacosDynamicServerListUpdater(NacosFactory.createNamingService(properties.getServerAddr()), properties);
//    }
//}
