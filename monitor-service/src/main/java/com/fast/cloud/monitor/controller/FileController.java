package com.fast.cloud.monitor.controller;

import com.fast.cloud.monitor.service.JStackService;
import com.fast.cloud.monitor.service.JmapService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * 文件下载
 */
@Slf4j
@Controller
@RequestMapping("/fast-actuator/file")
public class FileController{

    @Resource(name = "jmapService")
    private JmapService jmapService;

    @Resource(name = "jStackService")
    private JStackService jStackService;

    /**
     * 下载堆快照
     *
     * @param pid
     * @return
     * @throws IOException
     */
    @GetMapping("/heap")
    public ResponseEntity<byte[]> heapDump(@RequestParam("pid") String pid) throws IOException {
        String dump = jmapService.dump(pid);
        log.info("heapDump file {}:{}", pid, dump);
        File file = new File(dump);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", file.getName());
        return new ResponseEntity<>(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
    }

    /**
     * 下载线程快照
     *
     * @param pid
     * @return
     * @throws IOException
     */
    @GetMapping("/thread")
    public ResponseEntity<byte[]> threadDump(@RequestParam("pid") String pid) throws IOException {
        String dump = jStackService.jStackToFile(pid);
        File file = new File(dump);
        log.info("threadDump file {}:{}", pid, dump);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", file.getName());
        return new ResponseEntity<>(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
    }

}
